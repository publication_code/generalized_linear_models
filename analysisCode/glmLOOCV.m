% LOOCV with GLMs comparing step vs ramp models of evidence integration 
clear all; clc
cd('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode');
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode');
% init RNG
rng(1234,'twister') % default for reproducibility
bin_vector = 10; % binSize ms 
% LOOP animals
subject = ['b_fd';'n_fd';'b_rt'; 'n_rt'];
subject = cellstr(subject);
for binIterator = 1:length(bin_vector) 
    binSize = bin_vector(binIterator);
    save_folder = sprintf('../likelihoods/%d_timebin',binSize);  
    sprintf('bin size is %dms', binSize)
for subj_num = 1:length(subject) 
    tic
    cd(subject{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    % LOOP neurons
    for j = 1:length(matFiles)
        load(matFiles{j}); 
        % define output structs 
        stepModel = struct;
        rampModel = struct;
        % zero spike data to stimulus on
        rawspikes = data{3}(:,1);        % cell array of trials with spike times
        stimon = data{2}(:,34);          % stim on time for all trials; add to spike times
        stimoff = data{2}(:,35);         % stim off time for all trials
        sacctime = data{2}(:,37);        % saccade time
        spikes = cell(size(rawspikes));  % number of spikes
        correctTrials = data{2}(:,13);   % correct trials
        coherence = data{2}(:,5);        % coherence level for trials; includes NaNs
        % LOOP trials for current cell
        for i = 1:length(rawspikes) 
            i = 11; % TODO: 
            newStart = stimon(i); % TODO: stimulus on + 200ms
            if subject{subj_num}(end-1:end) == 'fd'
                newEnd = stimoff(i); 
            elseif subject{subj_num}(end-1:end) == 'rt'
                newEnd = sacctime - 50; % sacc time - 50ms for RT trials
            end            
            trialLength(i) = newEnd - newStart; % ms; stim off - stim on
            if newStart <=0 % pass these loop conditions
                disp(['trial ' num2str(i)])
                disp('Warning: recording started after stimulus on') 
                continue
            elseif isnan(newStart) 
                disp(['trial ' num2str(i)])
                disp('Warning: stimulus on time is NaN') 
                continue
            elseif isnan(coherence(i))
                disp(['trial ' num2str(i)])
                disp('Warning: coherence is NaN') 
                continue
            elseif trialLength(i) < 0 || isnan(trialLength(i))
                disp(['trial ' num2str(i)])
                disp('Warning: trialLength is negative or NaN') 
                continue
            elseif correctTrials(i) ~= 1
                disp(['trial ' num2str(i)])
                disp('Warning: this is an error trial') 
                continue
            else
                % get the spikes that are greater than ston, less than stoff
                temp = rawspikes{i}{:};
                spikes{i} = temp((temp>=newStart) & (temp<=newEnd));                
             end
            % zero onto ston 
            spikeTrain = spikes{i} - newStart;
            % convert to vector of spikes
            spikeVec = zeros(1,trialLength(i))';
            if isempty(spikeTrain)
                continue
            elseif spikeTrain(1) == 0
                spikeTrain(1) = 1; % perturb the first spike so that it fires at 1ms
                spikeVec(spikeTrain) = 1; 
            else
                spikevec(spikeTrain) = 1;
            end
            allTrains{i} = spikeTrain;
            % bin spikes
            histCentres = [0.5*binSize:binSize:trialLength(i)]; % time bin centres (ms)
            spikeHist = [hist(spikeTrain,histCentres)]; % bin spike trains  
            % basis functions for spike train history
            % [basisIdx,basis,bcenters]=makeSmoothTemporalBasis('boxcar',100,10,binSize);
            [basisIdx,spikeBasis,ctrs]=makeNonlinearRaisedCos(10, binSize, [0 10*binSize], 2); % nBases,binSize,1st/last peak in ms,nlOffset(bin sizes)    
            % LOOP ramp/step final time
            ll_ramp = [];
            ll_step = [];
            for tfinal = 1:length(spikeHist)
                % build integration models
                rampModel = [linspace(0,1,tfinal), ones(1,length(spikeHist)-tfinal)]; 
                stepModel = [zeros(1,tfinal-1), ones(1,length(spikeHist)-tfinal+1)]; 
                model = [rampModel; stepModel];
                for modelType = 1:2 % LOOP models
                    % design matrix
                    covariates(1).label = 'history filter';
                    covariates(1).basis = spikeBasis;
                    covariates(1).ts = spikeHist'; % need timeseries in tbin x dim format
                    covariates(1).offset = 1; % for spike hx; 1 timebin
                    covariates(1).convolve = 1;
                    covariates(2).label = 'spiking model';
                    covariates(2).basis = model(modelType,:)';
                    covariates(2).ts = model(modelType,:)';
                    covariates(2).convolve = [];
                    fullX = build_designMatrix(covariates,1); % using only 1 trial for now
%                     [fullX,zscore_mu,zscore_sigma] = zscore(fullX); % X - mean / stdev for each column
                    % w = (w .* zscore_sigma(:)) + zscore_mu(:); TODO:
                    % fullXDC = [ones(length(spikeHist),1), fullX]; % add bias column
                    % partition data 
                    cv = cvpartition(numel(spikeHist), 'LeaveOut');
                    ll = zeros(numel(spikeHist),1);       
                    for k=1:numel(spikeHist) % LOOP spikeHist bins for LOOCV
                        % training/testing indices for this fold
                        trainIdx = cv.training(k);
                        testIdx = cv.test(k);
                        % GLM prediction 
                        wts = glmfit(fullX(trainIdx,:),spikeHist(trainIdx),'poisson', 'constant', 'on');
                        const = wts(1);
                        % Compute predicted spike rate on training data
                        predRate= exp(const + fullX*wts(2:end)); % spikes in all bins; Y_hat
                        % compute log-likelihood
                        % Poisson LL is log P(s|r) =  s log r - r   
                        % ignored -log s! term because independent of parameters
                        % .The total log-likelihood is the summed log-likelihood over time bins in the experiment.
                        ll(k) = spikeHist(testIdx)*log(predRate(testIdx)+1e-10) - sum(predRate(testIdx)); % sum over only test bin
                        if isinf(ll(k)) % catch infs in LL
                            keyboard
                        end
                    end % end spikeHist bins
                    % MAP across tfinal for each model on current trial (i)
                    if modelType == 1
                        rampStats.tfinal{i} = find(ll == max(ll));
                        rampStats.trial_ll{i} = max(ll); 
                    elseif modelType == 2
                        stepStats.tfinal{i} = find(ll == max(ll));
                        stepStats.trial_ll{i} = max(ll);  
                    end
                end % end model type
            end % end tfinal
            keyboard
        end % end rawspikes
        % save analysis for current matFile/cell
        cd(save_folder)
        saveFile = strcat(matFiles{j}(1:end-4),'GLMfit');
        save(saveFile,'rampStats','stepStats')
        cd('../..')
        cd(subject{subj_num}); % cd to subject folder
    end % end matFiles
    cd('..') % exit from current subject folder 
    toc; keyboard

end % end subjects

end % end binSize


    
    













        % coherence, in/out-RF should be extra covariates
                
        % sudden jump in rate when stimon 
        % spike data 200ms after stimon -> 200m after stimoff
        % RT trials 200ms after stimon -> 50ms before saccade
        % estimated step dir not always matched to subject's decision 
        
        % Ramp model: 3 coh, init rate, slope per coh, noise
        % variance, upper bound, no lower bound (race model LIP), spike
        % trains obey inhomogeneous Poisson process for trajectory
        
        % initial rate (1st bin), random jump time per trial,
        % trajectory follows Poisson process given latent rate
        % neg. binomial distn ~ time-to-bound distn under diffusion model
        % params: spike rates for 3 states, 1 distn step timing, 1 dir for each motion coh 
        % both models: fit with spike trains, coherences, no access to animal choice
        
        % Pillow chose 40 neurons choice-selective, ramping in ave responses
        % out-RF choice leads to down ramp/step
      
        % ramp/step models have 3 overlapping GLMs in loop during LOOCV
        % what about no ramp/step? 1st/2nd bin same, Doesn't matter, they
        % would cancel!!!
        
%                 % Least squares for initialization
%                 tic
%                 wInit = fullXDC \ spikeHist'; % left matrix divide: X^-1 * y
%                 toc
%                 % Poisson regression
%                 fnlin = @expderivs; % inverse link function 
%                 lossfun = @(w)(loglikPoisson(w, fullXDC, spikeVec, fnlin)); 
%                 opts = optimoptions(@fminunc, 'Algorithm', 'trust-region','GradObj', 'on', 'Hessian','on');
%                 [wml, nlogli, exitflag, ostruct, grad, hessian] = fminunc(lossfun, wInit, opts);
%                 wvar = diag(inv(hessian)); % -H = obsFI(ML); inv(-H) = est. asympt. cov matrix -> sqrt(diag) est. SE
%                 ws = combineWeights(dm, wML); % combine weights across all stimuli, covariates     
             

                              