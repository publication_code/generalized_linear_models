% plots for step ramp paper 
figure; hold on 
rows = 3; % number of rows on figure

% ROW 1: step and ramp models
trialLength = 100; % number of 10ms bins
endtimes = [20, 30, 40, 50, 60];
constValues = [1,5,10,15,20]; 
time = [1:trialLength];
for i = 1:length(endtimes)
    rampModel(i,:) = [linspace(0,1,endtimes(i)), ones(1,trialLength-endtimes(i))]; 
    stepModel1(i,:) = [zeros(1,endtimes(i)), ones(1,trialLength-endtimes(i))]; 
    constModel(i,:) = [ones(1,trialLength) * constValues(i)];
end
subplot(rows,3,1)
ax1 = plot(time, rampModel,'markerSize',20);
ylim([0 1.1])
xlim([1 trialLength])
set(gca,'ytick',[]) 
xticks([1 100]) % bin count
xticklabels([0 850])
xlabel('time (ms)', 'FontSize', 18)
% text(0.02,0.98,'(a)','Units', 'Normalized', 'VerticalAlignment', 'Top')
subplot(rows,3,2)
% use same colors
co = get(gca,'ColorOrder'); % Initial color order
clrIdx = 1;
for chgpt = endtimes
    plot(time(1:chgpt),zeros(1,chgpt),'markerSize',20,'Color',co(clrIdx,:)); hold on
    plot([chgpt chgpt], [0 1],'markerSize',20,'Color',co(clrIdx,:));
    plot(time(chgpt:end),ones(trialLength-chgpt+1),'markerSize',20,'Color',co(clrIdx,:));
    clrIdx = clrIdx + 1;
end
xlim([1 trialLength])
ylim([0 1.1])
set(gca,'ytick',[]) 
xticks([1 100]) % bin count
xticklabels([0 850])
xlabel('time (ms)', 'FontSize', 18)
subplot(rows,3,3) 
ax3 = plot(time, constModel,'markerSize',20);
xlim([1 trialLength])
ylim([0 max(constValues)*1.1])
set(gca,'xtick',[],'ytick',[]) 
xlabel('time (ms)', 'FontSize', 18)

% text(0.02,0.98,'(c)','Units', 'Normalized', 'VerticalAlignment', 'Top')
    
    
% ROW 3: model comparisons 
% paths
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode')
origpath = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/';
loadpath = strcat(origpath,'stimonlyGLMresults/plot_data/');
cd(loadpath)
% load data
nohist_fd = load('fixed duration_nohist');
hist_fd = load('fixed duration_withhist');
nohist_rt = load('reaction time_nohist');
hist_rt = load('reaction time_withhist');
trialType = ['reaction time', 'fixed duration'];
covhist = ['nohist', 'withhist'];


% order by dprime find highest mean diffs
neuronIdx = dprime_order(nohist_fd.neurons_ll); % same as neuronIdx2 = dprime_order(hist_fd.neurons_ll)            
for plt = 1:2 % iterate through different model combos
    mainplt = subplot(rows,3,plt+6); miniplt = [];
    switch plt
        case 1 % constant - step
            model1Mean_nohist = nohist_fd.constMean(neuronIdx);
            model2Mean_nohist = nohist_fd.stepMean(neuronIdx);
            model1Var_nohist = nohist_fd.constVar(neuronIdx);
            model2Var_nohist = nohist_fd.stepVar(neuronIdx);
            model1Mean_hist = hist_fd.constMean(neuronIdx);
            model2Mean_hist = hist_fd.stepMean(neuronIdx);
            model1Var_hist = hist_fd.constVar(neuronIdx);
            model2Var_hist = hist_fd.stepVar(neuronIdx);
            nohist_barcolor = 'b';
            hist_barcolor = 'r';
            nohist_pdfshade = 'b'; 
            hist_pdfshade = 'r';
%             text(0.02,0.98,'(b)','Units', 'Normalized', 'VerticalAlignment', 'Top')
            [mainplt,miniplt] = plot_modelDiff(nohist_fd.neurons_ll,model1Mean_nohist,model2Mean_nohist,model1Var_nohist,model2Var_nohist,nohist_barcolor,nohist_pdfshade,mainplt,miniplt);
            [mainplt,miniplt] = plot_modelDiff(hist_fd.neurons_ll,model1Mean_hist,model2Mean_hist,model1Var_hist,model2Var_hist,hist_barcolor,hist_pdfshade,mainplt,miniplt);

        case 2 % constant - ramp
            model1Mean_nohist = nohist_fd.constMean(neuronIdx);
            model2Mean_nohist = nohist_fd.rampMean(neuronIdx);
            model1Var_nohist = nohist_fd.constVar(neuronIdx);
            model2Var_nohist = nohist_fd.rampVar(neuronIdx);
            model1Mean_hist = hist_fd.constMean(neuronIdx);
            model2Mean_hist = hist_fd.rampMean(neuronIdx);
            model1Var_hist = hist_fd.constVar(neuronIdx);
            model2Var_hist = hist_fd.rampVar(neuronIdx);
            nohist_barcolor = 'b';
            hist_barcolor = 'r';
            nohist_pdfshade = 'b'; % TODO
            hist_pdfshade = 'r';
            [mainplt,miniplt] = plot_modelDiff(nohist_fd.neurons_ll,model1Mean_nohist,model2Mean_nohist,model1Var_nohist,model2Var_nohist,nohist_barcolor,nohist_pdfshade,mainplt,miniplt);
            [mainplt,miniplt] = plot_modelDiff(hist_fd.neurons_ll,model1Mean_hist,model2Mean_hist,model1Var_hist,model2Var_hist,hist_barcolor,hist_pdfshade,mainplt,miniplt);
%         case 3 % ramp - step
%             model1Mean_nohist = nohist_fd.rampMean(neuronIdx);
%             model2Mean_nohist = nohist_fd.stepMean(neuronIdx);
%             model1Var_nohist = nohist_fd.rampVar(neuronIdx);
%             model2Var_nohist = nohist_fd.stepVar(neuronIdx);
%             model1Mean_hist = hist_fd.rampMean(neuronIdx);
%             model2Mean_hist = hist_fd.stepMean(neuronIdx);
%             model1Var_hist = hist_fd.rampVar(neuronIdx);
%             model2Var_hist = hist_fd.stepVar(neuronIdx);
%             nohist_barcolor = 'b';
%             hist_barcolor = 'r';
%             nohist_pdfshade = 'b'; % TODO
%             hist_pdfshade = 'r';
%             [mainplt,miniplt] = plot_modelDiff(nohist_fd.neurons_ll,model1Mean_nohist,model2Mean_nohist,model1Var_nohist,model2Var_nohist,nohist_barcolor,nohist_pdfshade,mainplt,miniplt);
%             [mainplt,miniplt] = plot_modelDiff(hist_fd.neurons_ll,model1Mean_hist,model2Mean_hist,model1Var_hist,model2Var_hist,hist_barcolor,hist_pdfshade,mainplt,miniplt);
    end
end



% ROW 2: example histogram plots 

% load data
neuroGLMpath = genpath('~/Programs/neuroGLM/');
addpath(neuroGLMpath)
binSize = 10; % ms
unitOfTime = 'ms';
subjects = ['b_fd';'n_fd'; 'b_rt';'n_rt']; 
subjects = cellstr(subjects);
models = cellstr({'ramp';'step';'constant'});
% covariate flags
hxFlag = []; % 'history'; 
integFlag = 'integrator';

% only fit FD, no Hx, fixed subj b_fd
% ramps: subj: b_fd neuron: b109d trial: 47
% TODO: need upward ramp

% steps: subj: b_fd neuron: b109b trial: 24
% const: subj: b_fd neuron: b108d trial: 14
neurons = ['b117d'; 'b109b'; 'b108d'];
neurons = cellstr(neurons);
trials = [6, 24, 14];

for plt = 1:3 
    subplot(rows,3,plt+3); hold on
%     text(0.02,0.98,'(c)','Units', 'Normalized', 'VerticalAlignment', 'Top')
    % load fitting results
    if strcmp(hxFlag, 'history')
        histFolder = 'withhist';
        histPrefix = 'history';
    elseif isempty(hxFlag)
        histFolder = 'nohist';
        histPrefix = [];
    end
    loadpath = strcat(origpath,'stimonlyGLMresults/',histFolder,'/10_timebin/',models{plt},'/',subjects{1});
    cd(loadpath)
%     keyboard
    load(strcat(neurons{plt},histPrefix,'integratorGLMresults_cv1',models{plt},'.mat'))
    % load original glm data
    cd(strcat(origpath,subjects{1}))
    load(neurons{plt})
    
    % recover all spike trains and trial indices
    glmData = buildGLM.updateSteptimes(glmData,binSize,bestt0,models{plt});
    [dm,yfull,trialIdx] = build_dsgnMat(unitOfTime,binSize,glmData,models{plt},hxFlag,integFlag);
    startIdx = trialIdx(trials(plt)) + 1; 
    endIdx = trialIdx(trials(plt)+1);
    predictedRate = exp(dm.X*wml);
    % bin spikes
    histCentres = [0.5*binSize:binSize:glmData.trial(trials(plt)).duration]; % time bin centres (ms)
    spikeHist = [hist(glmData.trial(trials(plt)).sptrain,histCentres)]; % bin spike trains  
    % plot smoothed histogram
    smoothedHist = smooth_spikes(spikeHist,'boxcar',10); % TODO: 
    bar(histCentres,smoothedHist/binSize,1.0,'k') % divide actual binned rate by binSize
    % predicted spike train
    if plt == 2
        stepRate = predictedRate(startIdx:endIdx);
        steptime = find(stepRate ~= stepRate(1),1,'first');
        plot(histCentres(1:steptime),stepRate(1)*ones(1,length(stepRate(1:steptime))),'r') % bottom rate
        plot(histCentres(steptime:end),stepRate(steptime)*ones(1,length(stepRate(steptime:end))),'r') % top rate
        plot([histCentres(steptime) histCentres(steptime)], [stepRate(1) stepRate(steptime)],'r'); % vertical line
    else
        plot(histCentres,predictedRate(startIdx:endIdx),'r')
    end

    % plot spike raster
%     rasterplt(glmData.trial(trials(plt)).sptrain) % TODO: test against yfull
    % format axes
    ylim([0 0.12])
    yticks([0 0.10])
    yticklabels([0 10])
    xlim([0 850]) % histCentres(end)])
    xticks([0 850])
    xlabel('time (ms)', 'FontSize', 18)
    ylabel('spikes (s^{-1})', 'FontSize', 18)
end

print -painters -depsc ~/Documents/Analysis/stepramp_comparison/figures/main_figure.eps 
% -svg

keyboard

% axis format     
ylim([1.5 3.5])
set(gca,'YTickLabel',[]);set(gca,'ytick',[]); set(gca, 'Ticklength', [0 0])
xlabel('ms'); 




% for two plots together
% a = [1:10];
% b = [1:10];
% figure(1)
% h1 = subplot(2,1,1);
% scatter(a,b);
% ylabel('data1')
% set(gca,'XTickLabel',[]);%remove Xaxis tick labels for h1
% h2 = subplot(2,1,2);
% scatter(a,b);
% xlabel('x axis data');
% ylabel('data2')
% p1 = get(h1, 'Position');
% p2 = get(h2, 'Position');
% p1(2) = p2(2)+p2(4);%change bottom starting point of h1
% set(h1, 'pos', p1);%set h1 position




















% spike train
trial = 11;
rawspikes = data{3}(:,1);        % cell array of trials with spike times
stimon = data{2}(:,34);          % stim on time for all trials; add to spike times
stimoff = data{2}(:,35);         % stim off time for all trials
sacctime = data{2}(:,37);        % saccade time
spikes = cell(size(rawspikes));  % number of spikes
correctTrials = data{2}(:,13);   % correct trials
coherence = data{2}(:,5);        % coherence level for trials; includes NaNs
figure;
for ii = 1:length(rawspikes{trial}{1}) % for every spike
    line([rawspikes{trial}{1}(ii) rawspikes{trial}{1}(ii)],[2.0 3.0],'Color','k'); % draw a black vertical line at time t (x) and at trial 180 (y)
end
% add lines for stimon,stimoff,sacctime
line([stimon(trial) stimon(trial)],[0 5],'Color','b');
line([stimoff(trial) stimoff(trial)],[0 5],'Color','b');
line([sacctime(trial) sacctime(trial)],[0 5],'Color','r');










% spike history basis functions
binSize = 10; % ms
[basisIdx,spikeBasis,ctrs]=makeNonlinearRaisedCos(10, binSize, [0 10*binSize], 2); % nBases,binSize,1st/last peak in ms,nlOffset(bin sizes)    
figure; hold on
plot(basisIdx, spikeBasis)
xlabel('ms')

% spike history filter

% ML model for ramp and spike imposed on bins

% plot spike rasters and fitted models 

% get full spike train 

% highlight portion fitted 

% overplot histogram 

% overplot Spike Hx filter 

% overplot null/ramp/step models



