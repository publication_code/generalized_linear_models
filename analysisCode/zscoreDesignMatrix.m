function dm = zscoreDesignMatrix(X)
% z-scores each column of the design matrix. Helps when ill-conditioned.
% dm = zscoreDesignMatrix(dm, colIndices);
%
% z-scoring removes the mean, and divides by the standard deviation.
%
% Input
%   X: matrix structure
%   colIndices: [n x 1] optional - indicies of columns you want to z-score
%	use getDesignMatrixColIndices to get indices from covariate labels
%
% Output
%   X : design matrix with z-scored columns
%	it has the meta data to correctly reconstruct weights later

    [X,zscore_mu,zscore_sigma] = zscore(X); % X - mean / stdev for each column

