function BX = temporalBases_dense(X, bases, indices, addDC)
% Computes the convolution of X with the selected bases functions.
% It cannot do partial time indices. Use it for trial-based computation.
%
% Each row of X marks events, and with corresponding bases selected by indices,
% it is convolved to the feature matrix.
%
% Input:
%   X: (T x dx) full matrix of events through time (T bins, dx covariate)
%   bases: (TB x M) TB bins of M bases
%   indices: (dx x M) binary index of which bases to use for each event
%   addDC: (boolean/default:false) if true, append a row of ones for DC (bias)
%
% Output:
%   BX: (T x nTotalFeatures) full matrix of features; time bins, nBases across all covariates (tiled as adjacent columns)
% 
% Notes: 
% conv2: two dim convolution C = conv2(A,B) [ma,na] = size(A), [mb,nb] = size(B), [mc,nc] = size(C)
% mc = max([ma+mb-1,ma,mb]) and nc = max([na+nb-1,na,nb])

if nargin < 4; addDC = 0; end % don't add DC bias
if addDC; addDC = 1; end

[T,dx] = size(X); % time bins, events
[TB, M] = size(bases); % time bins, bases

if nargin < 3; indices = true(dx, M); end % boolean true matrix (dx, M) use all bases for all events

sI = sum(indices, 2); % bases used for each covariate
BX = zeros(T, sum(sI) + addDC); % time bins, # total bases across all dims of current covariates

sI = cumsum(sI); k = 1; % sI: ending idx of bases for each covariate

for kCov = 1:dx % loop through covars
    A = conv2(X(:,kCov), bases(:,indices(kCov,:))); % take only relevant bases for current covariate 
    % 2D convolution of matrices stim(bins, covar) and bases(bins, bases to use)
    BX(:, k:sI(kCov)) = A(1:T,:); % 1:end-(nB-1)); take only first T time bins
    k = sI(kCov) + 1; % update to starting idx of next covariate's bases
end



if addDC; BX(:, end) = 1; end % DC column to right of matrix
