% build design matrix and weights for on Roitman and Shadlen, 2002 data
% clear all; clc
rng(1234,'twister') % default for reproducibility
binSize = 10; % binSize ms 
unitOfTime = 'ms';
sprintf('bin size is %dms', binSize)
kFolds = 10; 
modelType = 'step'; % ramp or step
% grad descent params
EMepsilon = 1000; % initialize
convergence = 1e-3; 
rho = 1.0; % ridge penalty
rhoNull = 1.0; % prior precision for other elements
% load trial folders
datapaths = genpath('~/Programs/neuroGLM/');
addpath('~/Programs/neuroGLM/matRegress/')
addpath(datapaths)
savefolder_prefix = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/fullGLMresults';
work_folder = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat';
cd(work_folder);
% LOOP folders
subjects = ['b_fd';'n_fd';'b_rt';'n_rt'];
subjects = cellstr(subjects);
for subj_num = 1:length(subjects) 
    save_folder = sprintf('%s/%d_timebin/%s/%s',savefolder_prefix,binSize,modelType,subjects{subj_num});  
    cd(subjects{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    % LOOP neurons
    for j = 1:length(matFiles)
        tic
        load(matFiles{j});
        % cross-validation indices
        cvIdx = {};
        for m = 1:glmData.nTrials 
            cvIdx{m} = crossvalind('Kfold', ceil(glmData.stimLengths(m)/binSize), kFolds); % 10-fold CV
        end
            % LOOP cross-val folds
            LLtestVec = []; % reset test LL vector
            for k = 1:kFolds       
                % LOOP EM 
                nllVec = []; % reset neg. LL vector
                emCounter = 1; % reset counter
                EMepsilon = 1000; % reset EMepsilon
                while EMepsilon > convergence
                    if isempty(nllVec)
                         % randomize t0s for each trial on the zeroth iteration
                        vect0 = round(rand(glmData.nTrials,1)'.*round(glmData.stimLengths/binSize)); 
                    else
                        vect0 = bestt0; % update t0 vector
                    end
                    % add updated t0 to trial fields for glmCV 
                    glmData = buildGLM.updateSteptimes(glmData,binSize,vect0);
                    % build design matrix 
                    [dm,y,endTrialIndices] = build_dsgnMat(unitOfTime,binSize,glmData,modelType);
                    % delete test bins
                    removeBins = [];
                    for trialIdx = 1:length(endTrialIndices)-1
                        foldIdx = find(cvIdx{trialIdx} == k); % trial indices for kth fold on this trial
                        trialFirstbin = endTrialIndices(trialIdx)+1;
                        removeBins = [removeBins; trialFirstbin+foldIdx];
                        endTrialIndices(trialIdx+1) = endTrialIndices(trialIdx+1) - length(removeBins);    
                    end
                    Ytest = y(removeBins);
                    dm.X(removeBins,:) = [];
                    y(removeBins) = [];
                    % calculate w_hat = argmax {w} p(spikes|t0_rand,w) 
                    [wml,dm,nlogli,exitflag] = findWeights(dm,y,rho,rhoNull,binSize);
%                     [wts1,dev,stats] = glmfit(dm.X,y,'poisson','link','log','constant','off'); 
                    % w_hat, t0_hat -> test convergence convergence
                    nllVec = [nllVec,  nlogli];
                    if emCounter > 1
                        EMepsilon = abs(nllVec(end) - nllVec(end-1)); 
                    end
                    emCounter = emCounter + 1;
                    % LOOP t0 scan 
                    for timeCounter = 1:ceil(max(glmData.stimLengths)/binSize)
                        % scan t0s for fixed w_hat
                        vect0 = timeCounter*ones(glmData.nTrials,1); 
                        resetIdx = ( vect0' > ceil(glmData.stimLengths/binSize) ); % compare with actual stimulus lengths 
                        vect0(resetIdx) = ceil(glmData.stimLengths(resetIdx)/binSize); % reset t0s that are greater than stimulus length last bin
                        % add updated t0 to trial fields
                        glmData = buildGLM.updateSteptimes(glmData,binSize,vect0);
                        % update design matrix 
                        [dm,~,~] = build_dsgnMat(unitOfTime,binSize,glmData,modelType);
                        dm.X(removeBins,:) = []; % remove test bins
                        % calculate LL per bin 
                        LLperbin = y.*(dm.X*wml+log(binSize)) - binSize*exp(dm.X*wml); % +ve log-likelihood of poisson GLM per bin 
                        % add LL across trial indices
                        LLperTrial = [];
                        for trialIdx = 1:length(endTrialIndices)-1
                            LLperTrial = [LLperTrial sum( LLperbin(endTrialIndices(trialIdx)+1:endTrialIndices(trialIdx+1)) )];
                        end
                        % place LLs into matrix
                        LLmatrix(timeCounter,:) = LLperTrial;
                    end % t0 scan
                    % save best t0_hat for each trial and for each iteration of t0 scan 
                    [maxLL, bestt0] = max(LLmatrix,[],1); % find max LL along rows (step times)
                end % convergence
                % test LL on held-out timebins
                % add updated t0 to trial fields
                glmData = buildGLM.updateSteptimes(glmData,binSize,bestt0);
                % update design matrix 
                [dm,~,~] = build_dsgnMat(unitOfTime,binSize,glmData,modelType);
                % extract test bins
                Xtest = dm.X(removeBins,:);
                LLtest = Ytest'*(Xtest*wml+log(binSize)) - binSize*sum(exp(Xtest*wml)); % +ve log-likelihood of poisson GLM on test bins                
                LLtestVec(end+1) = LLtest;
                if max(LLtestVec) == LLtest
                    % save best design matrix, weights, t0 for current neuron 
                    Xmat = dm.X;
                    cd(save_folder)
                    saveStr = strcat(matFiles{j}(1:end-4),'fullGLMfit_',modelType);
                    save(saveStr,'y','Xmat','wml','nlogli','LLtest','rho','bestt0') % wml, nlogli, rho, best t0
                end
                cd(work_folder)
            end % CV folds
            toc
            keyboard
            cd(subjects{subj_num})
    end % neurons
    cd(work_folder)
end % subjects 


% Pillow 2015: LIP model - fixation on, target on, dots off, coherence stimulus, saccade IN/OUT, spike history
% add other covariates: fix on, targ on, dots off
% each event is delta or boxcar convolved with kernel
% linear combo of basis functions: 2s after fixon, 1s after targon, 500ms
% after stimoff, saccade : 2 kernels, anticausal 2500ms
% dot stimulus: boxcar of corresponding duration, filter for each coh level
% 800ms. 
% coherence -ve values and bunch into 5 groups; +/- vals; zero coh trials could be some kind of test
% RT trials need to switch out dotsoff->saccade-> trgac
% different saccade kernels for IN/OUT
% Huk 2017 used 2500/100 bases for choice dep. saccades; also normalized;
% only use bins in CV during stimulus presentation
% need to assign +/- RF for step/ramp
% do last iteration after convergence? Probably should skip; t0 didn't change if last iteration cut

% regularization, cross-validation 
    % ridge parameter grid search/Laplace approx. ?? 
    % fit with 10-fold cross validation with ridge regularization; across timebins
    % Goodness of fit
% for saturated model ramp times tend to either beginning or end of trial
% x2 covariates for integrator, but choice agnostic (though implicitly true)
% x3 models; restrict to only stimulus time; remove begin/end spikes
    % - ramp/step; 2 covariates depending on in/out- RF
    % - ramp/step + spike hx; 2 covs for in/out- RF
    % - constant: scan different firing rates


%% Visualize fit
wvar = diag(inv(hessian));
ws = buildGLM.combineWeights(dm, wml); % Combine the weights per column in the design matrix per covariate (designmatrix, weight on basis fxns)
wvar = buildGLM.combineWeights(dm, wvar);

fig = figure(2914); clf;
nCovar = numel(dspec.covar);
for kCov = 1:nCovar
    label = dspec.covar(kCov).label;
    subplot(nCovar, 1, kCov);
    errorbar(ws.(label).tr, ws.(label).data, sqrt(wvar.(label).data)); % errorbar(x,y,e) 
    title(label);
end

