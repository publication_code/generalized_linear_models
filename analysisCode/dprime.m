function dprime(work_folder)
% returns whether neuron is choice selective
% based on d' measure in Pillow's ramps vs spikes paper
% d' = mu_in - mu_out / sqrt( 1/2 (sigma_in^2 + sigma_out^2) )
% in, out refer to RFs
% select top 50% of cells 
% Pillow applied this sieve only for 200-700ms after stimulus turned on


cd(work_folder);
% LOOP folders
trialType = ['fd'; 'rt'];
trialType = cellstr(trialType);
fd_subjects = cellstr(['b_fd';'n_fd']);
rt_subjects = cellstr(['b_rt';'n_rt']);

for task = 1:length(trialType)
    if strcmp(trialType(task),'fd')
        subjects = fd_subjects;
    elseif strcmp(trialType(task),'rt')
        subjects = rt_subjects;
    end
    % reset vectors
    trialTypematFiles = [];
    primeVec = [];
    inRateVec = [];
    outRateVec = [];
    mu_in = []; % mean spike counts on in-RF trials
    mu_out = []; % mean spike counts on out-RF trials
    var_in = []; % sigma_out^2
    var_out = []; 
    numTrials = [];
    for subj_num = 1:length(subjects) 
        cd(subjects{subj_num});
        files = dir;    
        matFiles = {};
        matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
        for cell = 1:length(matFiles)
            load(matFiles{cell});
            for trial = 1:glmData.nTrials 
                tmp = length(glmData.trial(trial).dprimeTrain); % < 500; % spike trains already restricted to appropriate range
                if glmData.trial(trial).inRF == 1
                    inRateVec(end+1) = sum(tmp) / glmData.trial(trial).dprimeLength*1e-3; % spike rate in sec 
                elseif glmData.trial(trial).inRF == -1
                    outRateVec(end+1) = sum(tmp) / glmData.trial(trial).dprimeLength*1e-3; 
                end
            end
            if isempty(inRateVec) || isempty(outRateVec)
                delete(matFiles{cell}); % delete current cell from folder
                matFiles{cell} =[];
                matFiles(~cellfun('isempty',matFiles)) % remove from matFiles
                continue
            end
            numTrials(end+1) = glmData.nTrials;
            mu_in(end+1) = mean(inRateVec);
            mu_out(end+1) = mean(outRateVec);
            var_in(end+1) = var(inRateVec);
            var_out(end+1) = var(outRateVec);
            primeVec(end+1) = ( mu_in(end) - mu_out(end) ) / sqrt( 1/2 * (var_in(end) + var_out(end)) ); 
            glmData.dprime = primeVec(end); % add dprime value to glmData
            save(matFiles{cell},'glmData'); % save updated glmData
        end % cell
        cd('../')        
        trialTypematFiles = [trialTypematFiles, matFiles]; % add matFiles after deletions have occurred
    end % subject
    % retain top 50% of cells for current trial type
    % give dprime value to each retained neuron 
    [sortedValues,sortIndex] = sort(primeVec,'ascend');  % Sort the values in descending order
    minIndex = sortIndex(1:ceil(length(primeVec)/2));  % Get bottom half of values
%     delete_matFiles(trialTypematFiles,minIndex,subjects)
%     primeVec(minIndex) = []; % remove lower half of values 
%     numTrials(minIndex) = [];
    display('Not deleting lower dprime cells!') % uncomment above lines to only run on highest dprime cells
    primeMin = min(primeVec)
    primeMax = max(primeVec)
    primeMean = mean(primeVec)
    primeStd = std(primeVec)
    numCells = length(primeVec)
    totalTrials = sum(numTrials)
end % trialType

