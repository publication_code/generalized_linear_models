function delete_matFiles(trialTypematFiles,index,subjects)

del_files = trialTypematFiles(index);

% loop subjects
for subj_num = 1:length(subjects)
    subj = subjects{subj_num};
    % cd into folder
    cd(subjects{subj_num});
    % get all matFiles in current folder
    files = dir;    
    matFiles = {};
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    % compare against list of matFiles to delete
    for file = 1:length(matFiles)
    	if any(strcmp(del_files,matFiles{file}))
            delete(matFiles{file})
        end
    end
    % cd out of folder
    cd('../')
end % subjects
