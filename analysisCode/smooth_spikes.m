function sdf = smooth_spikes(psth,ftype,w)
% [sdf kernel] = msdf(psth,ftype,w,varargin)
% make spike density function
% filters a psth to yield an SDF
%
% MANDATORY ARGUMENTS
% psth      2-column vector with time base (first column) and firing rate (second column) generated from mpsth
%           alternative: single column vector, time base lacking
% ftype     'Gauss' or 'boxcar' for a moving average window
% w         width of the filter kernel; for most purposes, reasonable values for w are 25-250 ms
%           - if ftype is 'boxcar', w denotes the width of the sliding window in ms; if w is an even number, the
%             program will instead use w+1
%           - if ftype is 'Gauss', w denotes the standard deviation (in ms), therefore 68% of the
%             spike mass will be within +- w ms

% 
% EXAMPLES
% msdf(psth,'exp',500)      returns a 2-column matrix with time base in the first column and the SDF in the second
%                           the SDF is filtered with an exponential kernel with a time constant of 500 ms



sdf = psth;  

switch ftype

  case 'boxcar'
    if ~mod(w,2)
      w = w + 1;
    end
    kernel      = ones(w,1)/(w);
    sdf         = conv(sdf,kernel,'same');
  
  case 'Gauss'
    Gauss_width = max([11 6*w+1]); % hm, should be an odd number... e.g. 11
    kernel      = normpdf(-floor(Gauss_width/2):floor(Gauss_width/2),0,w); % (values, mu, sigma)
    sdf         = conv(sdf,kernel,'same');
end
    