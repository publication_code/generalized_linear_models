% build design matrix and weights for on Roitman and Shadlen, 2002 data
% clear all; clc
% NB: this program doesn't find best t0 for each model type
rng(1234,'twister') % default for reproducibility
binSize = 10; % binSize ms 
unitOfTime = 'ms';
sprintf('bin size is %dms', binSize)
modelType = 'null'; 
% grad descent params
EMepsilon = 1000; % initialize
convergence = 1e-3; 
rho = 1.0; % ridge penalty
rhoNull = 1.0; % prior precision for other elements
% load trial folders
progpaths = genpath('~/Programs/neuroGLM/');
addpath(progpaths)
addpath('~/Programs/neuroGLM/matRegress/')
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode/')
savefolder_prefix = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/fullGLMresults';
work_folder = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/saturated_format';
cd(work_folder);
% LOOP folders
subjects = ['b_fd';'n_fd';'b_rt';'n_rt'];
subjects = cellstr(subjects);
for subj_num = 1:length(subjects) 
    save_folder = sprintf('%s/%d_timebin/%s/%s',savefolder_prefix,binSize,subjects{subj_num});  
    cd(subjects{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    % LOOP neurons
    for j = 1:length(matFiles)
        tic
        load(matFiles{j});
                    % build design matrix 
                    [dm,y,endTrialIndices] = build_dsgnMatsat(unitOfTime,binSize,glmData,modelType,'history',[]);
                    % calculate w_hat = argmax {w} p(spikes|t0_rand,w) 
                    [wml,dm,nlogli,exitflag] = findWeights(dm,y,rho,rhoNull,binSize);
                    % calculate LL per bin 
                    LLperbin = y.*(dm.X*wml+log(binSize)) - binSize*exp(dm.X*wml); % +ve log-likelihood of poisson GLM per bin 
                    % save design matrix, weights, t0 for current neuron 
                    Xmat = dm.X;
                    cd(save_folder)
                    saveStr = strcat(matFiles{j}(1:end-4),'fullGLMfit_',modelType);
                    save(saveStr,'y','Xmat','wml','nlogli','LLperbin','endTrialIndices','glmData') % wml, nlogli, rho, best t0
                    cd(work_folder)
                    cd(subjects{subj_num});
    end % neurons
    toc
    keyboard
end % subjects 
