% compare -log-likelihoods of step and ramp models 
% first summarize data from CV folds 
clear all; clc
trialType = {'fixed duration'}; %  'reaction time'};
fd_subjects = ['b_fd';'n_fd']; 
rt_subjects = ['b_rt';'n_rt'];
fd_subjects = cellstr(fd_subjects);
rt_subjects = cellstr(rt_subjects);
analyze_timebin = '10_timebin'; 
% models = {'constant' 'ramp' 'step'}; % for stimonlyGLMresults
models = {'ramp','step'}; % for stimonly_doublefit
histType = {'nohist','withhist'};
cv_folds = 10; 
% load data
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode/')
origpath = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/';
% savepath = strcat(origpath,'stimonlyGLMresults/plot_data/');
savepath = strcat(origpath,'stimonly_negpos/plot_data/');
for histcov = histType
%     datapath = strcat('~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/stimonlyGLMresults/',char(histcov),'/');
    datapath = strcat('~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/stimonly_negpos/',char(histcov),'/');
    for t = 1:length(trialType)
        if strcmp(trialType{t},'fixed duration')
            subjects = fd_subjects;
        elseif strcmp(trialType{t},'reaction time')
            subjects = rt_subjects;
        end
    for modelType = 1:length(models)
        display(sprintf('Analyzing %s and model type: %s', analyze_timebin,models{modelType}))
        neurons_ll = {}; % reset LL for neurons
        neuronCounter = 1;
        for subj = 1:length(subjects)
            cd(strcat(origpath,(subjects{subj})));   
            origFiles = dir;
            origmatFiles = {origFiles(~[origFiles.isdir]).name};
            cd(strcat(datapath,analyze_timebin,'/',models{modelType},'/',(subjects{subj})));   
            files = dir;
            matFiles = {files(~[files.isdir]).name};
            for i = 1:length(matFiles) % LOOP mat files
                try
                    load(matFiles{i})
                catch e
                    continue
                end
                matchStr = matFiles{i}(1:5);
                remainingFiles = matFiles(~cellfun('isempty',matFiles));  
                matchFiles = strfind(remainingFiles,matchStr); % finds all files that match current neuron    
                fileIdx = find(~cellfun(@isempty,matchFiles));
                cvFiles = remainingFiles(fileIdx);
                logLik = []; EM = [];
                for j = 1:length(cvFiles)
                    load(cvFiles{j})
                    logLik(end+1) = LLtest;
                    EM(end+1) = EMepsilon; 
                    % delete this matFile from list
                    index = find(strcmp(matFiles,cvFiles{j}));
                    matFiles{index} = [];
                end
                neurons_ll{neuronCounter}.logLik = logLik;
                neurons_ll{neuronCounter}.EM = EM; 
                neurons_ll{neuronCounter}.name = matchStr;
                cd(strcat(origpath,(subjects{subj})));   
                orig_GLMfile = strfind(origmatFiles,matchStr); % finds all files that match current neuron  
                origIdx = find(~cellfun(@isempty,orig_GLMfile));
                load(origmatFiles{origIdx});
                neurons_ll{neuronCounter}.nTrials = glmData.nTrials;
                neurons_ll{neuronCounter}.dprime = glmData.dprime; 
                neuronCounter = neuronCounter + 1;
                cd(strcat(datapath,analyze_timebin,'/',models{modelType},'/',(subjects{subj}))); % change back into matFile folder
            end
        end % subjects

        % calculate means and variances for each neuron 
        switch models{modelType}
            case 'ramp'
                [rampMean,rampVar,rampEM] = modelStats(neurons_ll);          
            case 'step'
                [stepMean,stepVar,stepEM] = modelStats(neurons_ll); 
        end   
    end % models

    % save 
    task = trialType{t};
%     save(strcat(savepath,task,'_',char(histcov)),'stepMean','rampMean','constMean','stepVar','rampVar','constVar','rampEM','stepEM','constEM','neurons_ll')
    save(strcat(savepath,task,'_',char(histcov)),'stepMean','rampMean','stepVar','rampVar','rampEM','stepEM','neurons_ll')
    % cd to working folder
    end % trialType
end % history covariate

% second plot comparisons

% paths
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode')
origpath = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/';
% paths from single model 
loadpath = strcat(origpath,'stimonlyGLMresults/plot_data/');
cd(loadpath)
% load constant model data from previous cross-val
nohist_origfd = load('fixed duration_nohist');
hist_origfd = load('fixed duration_withhist');

% paths for ramp/step + constant 
loadpath = strcat(origpath,'stimonly_negpos/plot_data/');
cd(loadpath)
% load data
nohist_fd = load('fixed duration_nohist');
hist_fd = load('fixed duration_withhist');
% nohist_rt = load('reaction time_nohist');
% hist_rt = load('reaction time_withhist');
trialType = ['reaction time', 'fixed duration'];
covhist = ['nohist', 'withhist'];


% order by dprime find highest mean diffs
figure; hold on
neuronIdx = dprime_order(nohist_fd.neurons_ll); % same as neuronIdx2 = dprime_order(hist_fd.neurons_ll)            
for plt = 1:2 % iterate through different model combos
    mainplt = subplot(1,2,plt); miniplt = [];
    switch plt
        case 1 % constant vs step
            model1Mean_nohist = nohist_origfd.constMean(neuronIdx);
            model2Mean_nohist = nohist_fd.stepMean(neuronIdx);
            model1Var_nohist = nohist_origfd.constVar(neuronIdx);
            model2Var_nohist = nohist_fd.stepVar(neuronIdx);
            model1Mean_hist = hist_origfd.constMean(neuronIdx);
            model2Mean_hist = hist_fd.stepMean(neuronIdx);
            model1Var_hist = hist_origfd.constVar(neuronIdx);
            model2Var_hist = hist_fd.stepVar(neuronIdx);
            nohist_barcolor = 'b';
            hist_barcolor = 'r';
            nohist_pdfshade = 'b'; 
            hist_pdfshade = 'r';
%             text(0.02,0.98,'(b)','Units', 'Normalized', 'VerticalAlignment', 'Top')
            [mainplt,miniplt] = plot_modelDiff(nohist_fd.neurons_ll,model1Mean_nohist,model2Mean_nohist,model1Var_nohist,model2Var_nohist,nohist_barcolor,nohist_pdfshade,mainplt,miniplt);
            [mainplt,miniplt] = plot_modelDiff(hist_fd.neurons_ll,model1Mean_hist,model2Mean_hist,model1Var_hist,model2Var_hist,hist_barcolor,hist_pdfshade,mainplt,miniplt);

        case 2 % constant vs ramp
            model1Mean_nohist = nohist_origfd.constMean(neuronIdx);
            model2Mean_nohist = nohist_fd.rampMean(neuronIdx);
            model1Var_nohist = nohist_origfd.constVar(neuronIdx);
            model2Var_nohist = nohist_fd.rampVar(neuronIdx);
            model1Mean_hist = hist_origfd.constMean(neuronIdx);
            model2Mean_hist = hist_fd.rampMean(neuronIdx);
            model1Var_hist = hist_origfd.constVar(neuronIdx);
            model2Var_hist = hist_fd.rampVar(neuronIdx);
            nohist_barcolor = 'b';
            hist_barcolor = 'r';
            nohist_pdfshade = 'b'; % TODO
            hist_pdfshade = 'r';
            [mainplt,miniplt] = plot_modelDiff(nohist_fd.neurons_ll,model1Mean_nohist,model2Mean_nohist,model1Var_nohist,model2Var_nohist,nohist_barcolor,nohist_pdfshade,mainplt,miniplt);
            [mainplt,miniplt] = plot_modelDiff(hist_fd.neurons_ll,model1Mean_hist,model2Mean_hist,model1Var_hist,model2Var_hist,hist_barcolor,hist_pdfshade,mainplt,miniplt);
%         case 3 % ramp - step
%             model1Mean_nohist = nohist_fd.rampMean(neuronIdx);
%             model2Mean_nohist = nohist_fd.stepMean(neuronIdx);
%             model1Var_nohist = nohist_fd.rampVar(neuronIdx);
%             model2Var_nohist = nohist_fd.stepVar(neuronIdx);
%             model1Mean_hist = hist_fd.rampMean(neuronIdx);
%             model2Mean_hist = hist_fd.stepMean(neuronIdx);
%             model1Var_hist = hist_fd.rampVar(neuronIdx);
%             model2Var_hist = hist_fd.stepVar(neuronIdx);
%             nohist_barcolor = 'b';
%             hist_barcolor = 'r';
%             nohist_pdfshade = 'b'; % TODO
%             hist_pdfshade = 'r';
%             [mainplt,miniplt] = plot_modelDiff(nohist_fd.neurons_ll,model1Mean_nohist,model2Mean_nohist,model1Var_nohist,model2Var_nohist,nohist_barcolor,nohist_pdfshade,mainplt,miniplt);
%             [mainplt,miniplt] = plot_modelDiff(hist_fd.neurons_ll,model1Mean_hist,model2Mean_hist,model1Var_hist,model2Var_hist,hist_barcolor,hist_pdfshade,mainplt,miniplt);
    end
end