function [basisIdx,basis,bcenters] = makeSmoothTemporalBasis(shape, duration, nBases, binSize)
%
% Input
%   shape: 'raised cosine' or 'boxcar'
%   duration: the time that needs to be covered
%   nBases: number of basis vectors to fill the duration
%   binfun: 
%
% Output
%   BBstm: basis vectors
%   

nkbins = ceil(duration/binSize); % number of bins for the basis functions

ttb = repmat((1:nkbins)', 1, nBases); % time/bin indices for each basis column; bins x Bases
if strcmpi(shape, 'raised cosine')
    % For raised cosine, the spacing between the centers must be 1/4 of the width of the cosine
    dbcenter = nkbins / (3 + nBases); % spacing between bumps
    width = 4 * dbcenter; % width of each bump
    % location of each bump centers
    bcenters = 2 * dbcenter + dbcenter*(0:nBases-1);
    bfun = @(x,period)((abs(x/period)<0.5).*(cos(x*2*pi/period)*.5+.5));
    BBstm = bfun(ttb-repmat(bcenters,nkbins,1), width);
elseif strcmpi(shape, 'boxcar')
    width = nkbins / nBases;
    BBstm = zeros(size(ttb));
    bcenters = width * (1:nBases) - width/2; % basis centers
    for k = 1:nBases
        idx = ttb(:, k) > ceil(width * (k-1)) & ttb(:, k) <= ceil(width * k); % indices of current boxcar; kth basis
        BBstm(idx, k) = 1 / sum(idx); % fo kth basis covers width with hieght 1/#bins; normalized boxcar height
    end
else
    error('Unknown basis shape');
end

basis = BBstm;
basisIdx = ttb-1;