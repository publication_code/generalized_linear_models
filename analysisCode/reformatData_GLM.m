% clean Roitman and Shadlen, 2002 data for use with neuroGLM 
clear all; clc
binSize = 10; %  ms
unitOfTime = 'ms';
% load trial folders
cd('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode');

% LOOP folders
subjects = ['b_fd';'n_fd';'b_rt'; 'n_rt'];
subjects = cellstr(subjects);

for subj_num = 1:length(subjects) 
    cd(subjects{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    for j = 1:length(matFiles)
        load(matFiles{j});
        % zero spike data to stimulus on       
        rawspikes = data{3}(:,1);        % cell array of trials with spike times
        stimon = data{2}(:,34);          % stim on time for all trials; add to spike times
        stimoff = data{2}(:,35);         % stim off time for all trials
        sacctime = data{2}(:,37);        % saccade time
        correctTrials = data{2}(:,13);   % correct trials
        coherence = data{2}(:,5);        % coherence level for trials; includes NaNs
        targon = data{2}(:,32);
        targoff = data{2}(:,33);         % NaNs
        fixon = data{2}(:,39);           % not jittered; always 0; fixation pt on
        fixoff = data{2}(:,36);
        targch = data{2}(:,12);          % sacc in/out based on target choice; all RF on T1
        if subjects{subj_num}(end-1:end) == 'rt'    
            % RT trials need to switch out stimoff->saccade-> trgac 
            stimoff = data{2}(:,37);  % stimoff -> saccade time
            sacctime = data{2}(:,38); % saccade -> target acquisition 
        end
        glmData = {};
        glmData.param = data{1}.file;
        glmData.nTrials = 0;
        glmData.stimLengths = [];
        glmData.trial = {};
        % LOOP trials 
        for i = 1:length(rawspikes)  
            trialLength(i) = sacctime(i) - fixon(i); % Memming used full trial, reward had little effect
            if stimon(i) <=0 % pass these loop conditions
                disp(['trial ' num2str(i)])
                disp('Warning: recording started after stimulus on') 
                continue
            elseif isnan(stimon(i)) 
                disp(['trial ' num2str(i)])
                disp('Warning: stimulus on time is NaN') 
                continue
            elseif isnan(coherence(i))
                disp(['trial ' num2str(i)])
                disp('Warning: coherence is NaN') 
                continue
            elseif trialLength(i) < 0 || isnan(trialLength(i))
                disp(['trial ' num2str(i)])
                disp('Warning: trialLength is negative or NaN') 
                continue
            elseif correctTrials(i) ~= 1
                disp(['trial ' num2str(i)])
                disp('Warning: this is an error trial') 
                continue
            elseif isempty(rawspikes{i}{1})
                disp(['trial ' num2str(i)])
                disp('Warning: this trial has an empty spike train')
            else
                temp = rawspikes{i}{1};
                spikeTrain = temp( (temp>=fixon(i)) );
                zerodspikeTrain = spikeTrain - fixon(i);
                if targch(i) == 1 % in receptive-field
                    inRF = 1; 
                else
                    inRF = -1;
                end
%                 histCentres = [0.5*binSize:binSize:zerodspikeTrain(end)]; % time bin centres (ms)
%                 spikeHist = [hist(spikeTrain,histCentres)]; % bin spike trains
                glmData.trial(glmData.nTrials+1).duration = trialLength(i);
                glmData.trial(glmData.nTrials+1).dotson = stimon(i) -fixon(i);
                glmData.trial(glmData.nTrials+1).dotsoff = stimoff(i)-fixon(i);
                glmData.stimLengths(end+1) = stimoff(i) - stimon(i)-fixon(i);
                glmData.trial(glmData.nTrials+1).targon = targon(i)-fixon(i);
                glmData.trial(glmData.nTrials+1).fixon = fixon(i)-fixon(i);
                glmData.trial(glmData.nTrials+1).fixoff = fixoff(i)-fixon(i);
                glmData.trial(glmData.nTrials+1).saccade = sacctime(i)-fixon(i);
                glmData.trial(glmData.nTrials+1).coh = inRF * coherence(i) * 1e-3; % coherences: 51.2,25.6,12.8,6.4,3.2,0 +/-
                glmData.trial(glmData.nTrials+1).choice = targch(i); % 1/2
                glmData.trial(glmData.nTrials+1).correct = correctTrials(i);
                glmData.trial(glmData.nTrials+1).inRF = inRF;
                glmData.trial(glmData.nTrials+1).sptrain = zerodspikeTrain;        
                glmData.nTrials = glmData.nTrials + 1;   
            end                
        end % trials
        % save trials for current neuron 
        save_name = sprintf('~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/saturated_format/%s/%s',subjects{subj_num},matFiles{j});  
        save(save_name,'glmData');
    end % neuron/matFile
    cd('../')
end % subjects


