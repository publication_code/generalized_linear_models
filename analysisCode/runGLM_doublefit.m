% build design matrix and weights for on Roitman and Shadlen, 2002 data
% for stimulus presentation period only
% both model and constant covariate are fit on an individual trial basis

% clear all; clc
rng(1234,'twister') % default for reproducibility
binSize = 10; % binSize ms 
unitOfTime = 'ms';
sprintf('bin size is %dms', binSize)
kFolds = 10; 
modelType = 'ramp' % ramp or step; constant covariate is fit for every  or constant
% covariate flags
hxFlag= []; % 'history' 
integFlag = 'integrator';
% max/min rates for constant covariate
maxRate = 100; 
minRate = 0;
% grad descent params
EMepsilon = 1000; % initialize
convergence = 1e-3;
rho = 1.0; % ridge penalty
rhoNull = 1.0; % prior precision for other elements
% load trial folders
datapaths = genpath('~/Programs/neuroGLM/');
addpath('~/Programs/neuroGLM/matRegress/')
addpath(datapaths)
savefolder_prefix = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/stimonly_doublefit'; 
work_folder = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat';
% subjects
subjects = ['b_fd';'n_fd';'b_rt';'n_rt'];
subjects = cellstr(subjects);
% delete previous analysis
for subj_num = 1:length(subjects)
    save_folder = sprintf('%s/%d_timebin/%s/%s',savefolder_prefix,binSize,modelType,subjects{subj_num});      
    cd(save_folder);
    delete('*.mat')
end
cd(work_folder);


% LOOP folders
for subj_num = 1:length(subjects) 
    save_folder = sprintf('%s/%d_timebin/%s/%s',savefolder_prefix,binSize,modelType,subjects{subj_num});  
    cd(subjects{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    % LOOP neurons
    for j = 1:length(matFiles)
        tic
        load(matFiles{j});
        % cross-validation indices
        cvIdx = {};
        for m = 1:glmData.nTrials 
            cvIdx{m} = crossvalind('Kfold', ceil(glmData.stimLengths(m)/binSize), kFolds); % 10-fold CV
        end
        LLtestVec = []; % reset test LL vector
            % LOOP cross-val folds
            for k = 1:kFolds       
                % LOOP EM 
                nllVec = []; % reset neg. LL vector
                emCounter = 1; % reset counter
                EMepsilon = 1000; % reset EMepsilon
                while 1
                    if isempty(nllVec) 
                        % randomize t0s for each trial on the zeroth iteration
                        vecT0 = round(rand(glmData.nTrials,1)'.*round(glmData.stimLengths/binSize)); 
                        % randomize constant firing rates
                        vecR0 = randi([minRate maxRate],1,glmData.nTrials);  
                    else
                        vecT0 = bestT0; % update t0 vector
                        vecR0 = bestR0;
                    end
                    
                    % add updated t0,r0 to trial fields for glmCV 
                    glmData = buildGLM.doubleUpdate(glmData,binSize,vecT0,vecR0); % update t0, r0
%                     glmData = buildGLM.updateSteptimes(glmData,binSize,vecT0,modelType);
                    
                    % build design matrix 
                    [dm,y,endTrialIndices] = build_dmat_doublefit(unitOfTime,binSize,glmData,modelType,hxFlag,integFlag);
                    % delete test bins
                    removeBins = [];
                    new_endTrialIndices = [0];
                    for trialIdx = 1:length(endTrialIndices)-1
                        foldIdx = find(cvIdx{trialIdx} == k); % trial indices for kth fold on this trial
                        trialLastbin = endTrialIndices(trialIdx);
                        removeBins = [removeBins; trialLastbin+foldIdx];
                        new_endTrialIndices(trialIdx+1) = endTrialIndices(trialIdx+1) - length(removeBins);    
                    end
		    endTrialIndices = new_endTrialIndices; % update end of trial indices
		    % test for duplicate indices
		    [c,ia,ic] = unique(removeBins);
 		    duplicate_ind = setdiff(1:size(removeBins,1), ia);
		    if ~isempty(duplicate_ind)
		    	display('duplicate trial indices detected.')
                keyboard
		    end
                    Ytest = y(removeBins);
                    dm.X(removeBins,:) = [];
                    y(removeBins) = [];
                    % calculate w_hat = argmax {w} p(spikes|t0_rand,w) 
                    [wml,dm,nlogli,exitflag] = findWeights(dm,y,rho,rhoNull,binSize);
                    % w_hat, t0_hat -> test convergence convergence
                    nllVec = [nllVec,  nlogli];
                    if emCounter > 1
                        EMepsilon = abs(nllVec(end) - nllVec(end-1));
                    end
                    
                    if EMepsilon < convergence || emCounter > 30
                        break
                    end
                    emCounter = emCounter + 1;
                    
                   % LOOP r0,t0 scans 
                    LL_r0matrix = [];
                    
                    for rateCounter = minRate:maxRate
                        vecR0 = rateCounter*ones(glmData.nTrials,1); 
                        % add updated t0 to trial fields
                        glmData = buildGLM.doubleUpdate(glmData,binSize,vecT0,vecR0); % update t0, r0                        
%                         glmData = buildGLM.updateSteptimes(glmData,binSize,vect0,modelType);
                        % update design matrix 
                        [dm,~,~] = build_dmat_doublefit(unitOfTime,binSize,glmData,modelType,hxFlag,integFlag);
                        dm.X(removeBins,:) = []; % remove test bins
                        % calculate LL per bin 
                        LLperbin = y.*(dm.X*wml+log(binSize)) - binSize*exp(dm.X*wml); % +ve log-likelihood of poisson GLM per bin 
                        % add LL across trial indices
                        LLperTrial = [];
                        for trialIdx = 1:length(endTrialIndices)-1
                            LLperTrial = [LLperTrial sum( LLperbin(endTrialIndices(trialIdx)+1:endTrialIndices(trialIdx+1)) )];
                        end
                        % place LLs into matrix
                        LL_r0matrix(end+1,:) = LLperTrial;
                    end % end r0 scan

                    LL_t0matrix = [];
                    for timeCounter = 1:ceil(max(glmData.stimLengths)/binSize)          
                        % scan t0s for fixed w_hat
                        vecT0 = timeCounter*ones(glmData.nTrials,1); 
                        resetIdx = ( vecT0' > ceil(glmData.stimLengths/binSize) ); % compare with actual stimulus lengths 
                        vecT0(resetIdx) = ceil(glmData.stimLengths(resetIdx)/binSize); % reset t0s that are greater than stimulus length last bin
                        % add updated t0 to trial fields
                        glmData = buildGLM.doubleUpdate(glmData,binSize,vecT0,vecR0); % update t0, r0                                                
%                         glmData = buildGLM.updateSteptimes(glmData,binSize,vect0,modelType);
                        % update design matrix 
                        [dm,~,~] = build_dmat_doublefit(unitOfTime,binSize,glmData,modelType,hxFlag,integFlag);
                        dm.X(removeBins,:) = []; % remove test bins
                        % calculate LL per bin 
                        LLperbin = y.*(dm.X*wml+log(binSize)) - binSize*exp(dm.X*wml); % +ve log-likelihood of poisson GLM per bin 
                        % add LL across trial indices
                        LLperTrial = [];
                        for trialIdx = 1:length(endTrialIndices)-1
                            LLperTrial = [LLperTrial sum( LLperbin(endTrialIndices(trialIdx)+1:endTrialIndices(trialIdx+1)) )];
                        end
                        % place LLs into matrix
                        LL_t0matrix(timeCounter,:) = LLperTrial;
                    end % t0 scan
                    
                    % save best t0_hat for each trial and for each iteration of t0 scan 
                    % NB: assumes r0 and t0 begin from 1 Hz, 1st bin respectively
                    [maxLL_r0, bestR0] = max(LL_r0matrix,[],1); % find max LL along rows (step times)
                    % save best r0_hat for each trial and for each iteration of r0 scan 
                    [maxLL_t0, bestT0] = max(LL_t0matrix,[],1); % find max LL along rows (step times)
                    % reset bestt0 that is over final timebin to last bin
                    resetIdx = ( bestT0 > ceil(glmData.stimLengths/binSize) ); % compare with actual stimulus lengths 
                    bestT0(resetIdx) = ceil(glmData.stimLengths(resetIdx)/binSize); % reset t0s that are greater than stimulus length last bin
                end % convergence
                
                % test LL on held-out timebins
                % add updated r0,t0 to trial fields
                glmData = buildGLM.doubleUpdate(glmData,binSize,bestT0,bestR0); % update t0, r0                                                
                % update design matrix 
                [dm,~,~] = build_dmat_doublefit(unitOfTime,binSize,glmData,modelType,hxFlag,integFlag);
                % extract test bins
                Xtest = dm.X(removeBins,:);
                LLtest = Ytest'*(Xtest*wml+log(binSize)) - binSize*sum(exp(Xtest*wml)); % +ve log-likelihood of poisson GLM on test bins                
                LLtestVec(end+1) = LLtest;
                % save best design matrix, weights, r0, t0 for current neuron 
                Xmat = dm.X;
                dprime = glmData.dprime;
                cd(save_folder)
                saveStr = strcat(matFiles{j}(1:end-4),hxFlag,integFlag,'GLMresults_cv',int2str(k),modelType);
                save(saveStr,'y','Xmat','wml','LLtest','rho','dprime','bestT0','bestR0','EMepsilon') % wml, nlogli, rho, best t0,r0
                cd(work_folder)
                keyboard
            end % CV folds
            toc
            cd(subjects{subj_num})
    end % neurons
    keyboard
    cd(work_folder)
end % subjects 


% exclude windows of spikes according to Pillow paper
% We focused on a population of 40 neurons with highly choice-selective responses that exhibited ramping in their average responses (12), 
% typically increasing during trials in which the monkey eventually chose the target inside the response field (RF) of the neuron, 
% and decreasing when the monkey chose the target outside the RF. We fit each neuron with both ramping and stepping models, 
% using the spike train data from 200 ms after motion onset (Kiani2008) until 200 ms after motion offset (300 ms before the monkey received the go signal). 

% TODO: deviance -> DIC/BIC 

% Pillow uses d' measure to exclude neurons; don't think he studied error trials; coh 

% we should use the entire integration period; but exlcude first 200ms
% we do not have a diffusion component
% assumes uniform distribution of step times 
% we do not in general step/ramp directions to be corr'd with choice
% implies that a single GLM weight won't suffice for all trials on ramps;
% or ramps need to find different heights as well.

% Roitman2002 no go signal; Pillow applied analysis 200ms after motion
% onset -> saccade - 50ms; trials > 350ms from stimon -> saccade (at least
% 100ms of data); =>8 trials/signed coh level (x5 levels)

