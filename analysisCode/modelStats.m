function [meanVec,varVec,EMres] = modelStats(neurons_ll)
% compute mean and variance of cross-validation folds for all neurons


for i = 1:length(neurons_ll)
    
    meanVec(i) = mean(neurons_ll{i}.logLik);

    varVec(i) = var(neurons_ll{i}.logLik);
    
    EMres{i} = neurons_ll{i}.EM;
    
end
    