function X = convBasis(stim, bases, offset)
% Convolve basis functions to the covariate matrix
%
%   stim: [T x dx] - dx covariates over time
%   bases: Basis structure
%   offset: [1] optional - shift in time
%     X: designmat convolved with basis functions

if nargin < 3
    offset = 0;
end

[~, dx] = size(stim);  % dx is dimension 

% zero pad stim to account for offset
if offset < 0 % anti-causal
    stim = [stim; zeros(-offset, dx)]; % append zeros to end of vertical cols
elseif offset > 0; % push to future
    stim = [zeros(offset, dx); stim]; % append zeros to start of vertical cols
end

X = temporalBases_dense(stim, bases); % doesn't include DC column

if offset < 0 % anti-causal
    X = X(-offset+1:end, :); % lops offset from the beginning
elseif offset > 0
    X = X(1:end-offset, :); % cut off offset from the end
end