function [neuronIdx] = dprime_order(neurons_ll)
% given a result struct for fd/rt and withhist/nohist return indices of
% neurons ordered by their dprime

dprimeVec = [];
for neuron = neurons_ll
    dprimeVec(end+1) = neuron{:}.dprime;
end

[~,neuronIdx] = sort(dprimeVec,'descend');

