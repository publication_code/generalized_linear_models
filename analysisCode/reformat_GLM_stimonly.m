% clean Roitman and Shadlen, 2002 data for use with neuroGLM 
% column names are derived from data{1}.trialfields
clear all; clc
binSize = 10; %  ms
unitOfTime = 'ms';
% define folders
home_folder = '~/Documents/Analysis/stepramp_comparison/';
addpath(sprintf(('%sRoitmanDataCode/'),home_folder));
save_folder = sprintf('%s/RoitmanData_GLMformat',home_folder);

% offsets for stimulus duration of interest (Pillow, 2015)
stimon_offset1 = 200; % ms
stimon_offset2 = 700; % ms
saccade_offset = 50; % ms
% LOOP folders
subjects = ['b_fd';'n_fd' ;'b_rt'; 'n_rt'];
subjects = cellstr(subjects);

% delete previously saved trial data
for subj_num = 1:length(subjects)
    % save trials for current neuron 
    subjdata_folder = sprintf('%s/RoitmanData_GLMformat/%s',home_folder,subjects{subj_num});  
    cd(subjdata_folder);
    delete('*.mat')
end
cd(sprintf('%sRoitmanDataCode',home_folder));

for subj_num = 1:length(subjects)  
    cd(subjects{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    for j = 1:length(matFiles)
        load(matFiles{j});
        % zero spike data to stimulus on       
        rawspikes = data{3}(:,1);        % cell array of trials with spike times
        stimon = data{2}(:,34) + stimon_offset1;          % stim on time for all trials; add to spike times
        stimoff = data{2}(:,35);         % stim off time based on stimon + 700ms
        sacctime = data{2}(:,37) - saccade_offset;        % saccade time
        correctTrials = data{2}(:,13);   % correct trials
        coherence = data{2}(:,5);        % coherence level for trials; includes NaNs
        targon = data{2}(:,32);
        targoff = data{2}(:,33);         % NaNs
        fixon = data{2}(:,39);           % not jittered; always 0; fixation pt on
        fixoff = data{2}(:,36);
        targch = data{2}(:,12);          % sacc in/out based on target choice; all RF on T1
        dotdir = data{2}(:,6);           % direction of dots, 0 - zero coherence
        if subjects{subj_num}(end-1:end) == 'rt'    
            % RT trials need to switch out stimoff->saccade-> trgac 
            % stimoff = data{2}(:,37);  % stimoff -> saccade time
            stimoff = data{2}(:,38) - saccade_offset; % saccade -> target acquisition (38)
        end
        glmData = {};
        glmData.param = data{1}.file;
        glmData.nTrials = 0;
        glmData.stimLengths = [];
        glmData.trial = {};
        % LOOP trials 
        for i = 1:length(rawspikes)  
            trialLength(i) = stimoff(i) - stimon(i);
            if stimon(i) - stimon_offset1 <= 0 % pass these loop conditions
                % test original stimon time
                disp(['trial ' num2str(i)])
                disp('Warning: recording started after stimulus on') 
                continue
            elseif isnan(stimon(i)) 
                disp(['trial ' num2str(i)])
                disp('Warning: stimulus on time is NaN') 
                continue
            elseif isnan(coherence(i))
                disp(['trial ' num2str(i)])
                disp('Warning: coherence is NaN') 
                continue
            elseif trialLength(i) < 100 || isnan(trialLength(i)) % for RT trials
                disp(['trial ' num2str(i)])
                disp('Warning: trialLength is negative or NaN') 
                continue
            elseif correctTrials(i) ~= 1
                disp(['trial ' num2str(i)])
                disp('Warning: this is an error trial')         
                continue
            elseif isempty(rawspikes{i}{1})
                disp(['trial ' num2str(i)])
                disp('Warning: this trial has an empty spike train')
            else
                temp = rawspikes{i}{1};
                spikeTrain = temp( (temp>=stimon(i) & temp<=stimoff(i)) );
                zerodspikeTrain = spikeTrain - stimon(i);
                if targch(i) == 1 % in receptive-field
                    inRF = 1; 
                else
                    inRF = -1;
                end 

                glmData.trial(glmData.nTrials+1).duration = trialLength(i);
                glmData.trial(glmData.nTrials+1).dotson = stimon(i) -stimon(i); % duration of interest stimon+200ms
%                 glmData.trial(glmData.nTrials+1).dotsoff = stimoff(i);
                glmData.stimLengths(end+1) = stimoff(i) - stimon(i); % already takes into account left out portions; same as trialLength
                if strcmp(subjects{subj_num}(end-1:end),'fd')
                    glmData.trial(glmData.nTrials+1).dprimeLength = stimon(i) + 500; % 200-700ms for d' calc
                    glmData.trial(glmData.nTrials+1).dprimeTrain = zerodspikeTrain(zerodspikeTrain <= 500); % take only spikes within 200-700ms window
                    glmData.trial(glmData.nTrials+1).rawspikes = rawspikes{i}{1}; % only req'd for FD trials
                    glmData.trial(glmData.nTrials+1).origstimon = data{2}(i,34);      % only req'd for FD trials
                    glmData.trial(glmData.nTrials+1).origstimoff = data{2}(i,35);      % only req'd for FD trials
                    glmData.trial(glmData.nTrials+1).sacctime = data{2}(i,37);      % only req'd for FD trials
                else strcmp(subjects{subj_num}(end-1:end),'rt')
                    glmData.trial(glmData.nTrials+1).dprimeLength = trialLength(i); % 200 to saccade-50ms for d' calc
                    glmData.trial(glmData.nTrials+1).dprimeTrain = zerodspikeTrain; % take only spikes within above window
                end
%                 glmData.trial(glmData.nTrials+1).targon = targon(i);
%                 glmData.trial(glmData.nTrials+1).fixon = fixon(i);
%                 glmData.trial(glmData.nTrials+1).fixoff = fixoff(i);
%                 glmData.trial(glmData.nTrials+1).saccade = sacctime(i);
                glmData.trial(glmData.nTrials+1).coh = coherence(i); % coherences: 51.2,25.6,12.8,6.4,3.2,0 +/-
                glmData.trial(glmData.nTrials+1).dotdir = dotdir(i);
                glmData.trial(glmData.nTrials+1).choice = targch(i); % 1/2
                glmData.trial(glmData.nTrials+1).correct = correctTrials(i);
                glmData.trial(glmData.nTrials+1).inRF = inRF;
                glmData.trial(glmData.nTrials+1).sptrain = zerodspikeTrain;        
                glmData.nTrials = glmData.nTrials + 1;   
            end                
        end % trials
        if glmData.nTrials > 3 % exclude neurons with insufficient number of trials to fit
            % save glmData for current neuron 
            save_name = sprintf('../../RoitmanData_GLMformat/%s/%s',subjects{subj_num},matFiles{j});  
            save(save_name,'glmData');
        end
    end % neuron/matFile
    cd('../')
end % subjects

% apply Pillow's d' to find choice selective neurons 
dprime(save_folder);



