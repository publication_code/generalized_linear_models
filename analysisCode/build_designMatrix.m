function fullX = build_designMatrix(covariates,numTrials) 
% % build design matrix
% covariates: struct with timeseries, offset, basis
totalDims = 0;
idxCounter = 1;
for ax = 1:length(covariates)
    totalDims = totalDims + size(covariates(ax).basis,2);
end
fullX = zeros(0,totalDims);
% LOOP trials for current cell 
for trial = 1:numTrials
    nBins = size(covariates(1).ts,1); % bins in current trial
    miniX = zeros(nBins,totalDims); % mini dsgnMat
    % LOOP covariates
    for kCov = 1:numel(covariates)
        covar = covariates(kCov);
        lengthBasis = size(covar.basis,2); 
        if ~isempty(covar.convolve)
            miniX(:, idxCounter:lengthBasis+idxCounter-1) = convBasis(covar.ts,covar.basis,covar.offset); % tbins x bases across all covars
            % convolve basis functions to the covariate matrix
        else
            miniX(:, idxCounter:lengthBasis+idxCounter-1) = covar.ts; % covariate ts with no basis convolution
        end
        idxCounter = idxCounter + lengthBasis;
    end
    fullX = [fullX; miniX]; 
end





% basis fxns for ramp / step? does it make sense to have a diff set of wgts for each trial?


% spike history basis
% offset = 2; % Make sure to be causal 1 bin. No instantaneous interaction allowed for spike trains
% spikehxBasis = makeNonlinearRaisedCos(10,binSize,[0 100],offset); % nBases,binSize,1st/last peak,nlOffset=numBins
% % coherence basis
% cohBasis = makeSmoothTemporalBasis('raised cosine', 200, 10, binfun);
% 
% stimHandle = @(trial, expt) trial.coh * basisFactory.boxcarStim(binfun(trial.dotson), binfun(trial.dotsoff), binfun(trial.duration));
% dspec = buildGLM.addCovariate(dspec, 'cohKer', 'coh-dep dots stimulus', stimHandle, bs);

