function plt_glmfit()
% plot GLM fits to trials of neurons to find best fitting trials

% iterate through trials of all neurons to find good fits 
% load data
neuroGLMpath = genpath('~/Programs/neuroGLM/');
addpath(neuroGLMpath)
origpath = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/';
binSize = 10; % ms
unitOfTime = 'ms';
subjects = ['b_fd';'n_fd'; 'b_rt';'n_rt']; 
subjects = cellstr(subjects);
models = cellstr({'ramp';'step';'constant'});
model = models{3}; 
% covariate flags
hxFlag = []; 
integFlag = 'integrator';


% fix models

for subj=1:length(subjects)  
    cd(origpath)
    cd(subjects{subj});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs   
    for neuron=matFiles
        load(neuron{1}) % to get glmData
        % change load path
        loadpath = strcat(origpath,'stimonlyGLMresults/','nohist/10_timebin/',model,'/',subjects{subj});
        cd(loadpath)
        load(strcat(neuron{1}(1:end-4),'integratorGLMresults_cv1',model,'.mat'))
        glmData = buildGLM.updateSteptimes(glmData,binSize,bestt0,model);
        [dm,yfull,trialIdx] = build_dsgnMat(unitOfTime,binSize,glmData,model,hxFlag,integFlag); % to get trialindices
        predictedRate = exp(dm.X*wml);
        for trial=1:glmData.nTrials
            figure; 
            startIdx = trialIdx(trial) + 1; % starts with 0 
            endIdx = trialIdx(trial+1); % trialIdx has nTrials+1 entries
           %     rasterplt(glmData.trial(trials(plt)).sptrain) % TODO: test against yfull
            histCentres = [0.5*binSize:binSize:glmData.trial(trial).duration]; % time bin centres (ms)
            spikeHist = [hist(glmData.trial(trial).sptrain,histCentres)]; % bin spike trains  
            % plot actual sptrain
            smoothedHist = smooth_spikes(spikeHist,'boxcar',10);
            plot(smoothedHist/binSize,'b')
            % predicted spike train
            hold on
            plot(predictedRate(startIdx:endIdx),'r')
            title(sprintf(('subject: %s, neuron: %s, trial: %d'),subjects{subj},neuron{1},trial))
        end
        keyboard
        cd(origpath)
        cd(subjects{subj});
    end
end




% const: subj: b_fd neuron: b108d trial: 14,18,41
% use trial 14

% ramps: subj: b_fd neuron: b109d-29, b112b-1, b117d-6,18,19 
% use b_fd, b109b, trial: 47

% steps: b109b-24,26,36,41,45 b109b-41, 108h-38,
% use b109b-24