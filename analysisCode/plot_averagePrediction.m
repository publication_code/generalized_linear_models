% plot comparison of averaged PSTH vs. model predictions 
% plot fano factor for all neurons collapsed by coherence and RF
clear all; clc
% paths
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode')
origpath = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/';
loadpath = strcat(origpath,'stimonlyGLMresults/plot_data/');
cd(loadpath)

neuroGLMpath = genpath('~/Programs/neuroGLM/');
addpath(neuroGLMpath)
binSize = 10; % ms
unitOfTime = 'ms';
subjects = ['b_fd';'n_fd'; 'b_rt';'n_rt']; 
subjects = cellstr(subjects);
models = cellstr({'ramp';'step';'constant'});
% covariate flags
hxFlag = []; % 'history'; 
integFlag = 'integrator';
% load fitting results
if strcmp(hxFlag, 'history')
    histFolder = 'withhist';
    histPrefix = 'history';
elseif isempty(hxFlag)
    histFolder = 'nohist';
    histPrefix = [];
end
modelClrs = ['r';'b';'g'];
modelClrs = cellstr(modelClrs);



    
%                 glmData.trial(glmData.nTrials+1).dotdir = dotdir(i);
%                 glmData.trial(glmData.nTrials+1).correct = correctTrials(i);

    
for modelType = 1:length(models)
        % spike histogram and model prediction matrices
        if modelType ~= 1
            clearvars spikeHist spikeHist_inAvg spikeHist_outAvg modelHist modelHist_inAvg modelHist_outAvg;
        end
        spikeHist.inRF = [];
        spikeHist.outRF = [];
         spikeHist(6).inRF = [];
        spikeHist_inAvg = {};
        spikeHist_outAvg = {};
        spikeHist_inAvg{6} = [];
        spikeHist_outAvg{6} = [];
        modelHist.inRF = [];
        modelHist.outRF = [];
        modelHist(6).inRF = [];
        modelHist(6).outRF = [];
        modelHist_inAvg = {};
        modelHist_outAvg = {};
        loadpath = strcat(origpath,'stimonlyGLMresults/',histFolder,'/10_timebin/',models{modelType},'/',subjects{1});
        cd(loadpath)
        load(strcat('b108d',histPrefix,'integratorGLMresults_cv1',models{modelType},'.mat')) % loads bestt0 etc

        % load original glm data
        cd(strcat(origpath,subjects{1})) % original dataset
        load('b108d')
        % recover all spike trains and trial indices
        glmData = buildGLM.updateSteptimes(glmData,binSize,bestt0,models{modelType});
        [dm,yfull,trialIdx] = build_dsgnMat(unitOfTime,binSize,glmData,models{modelType},hxFlag,integFlag);
        % bins for spikes; this is constant for FD trials
        histCentres = [0.5*binSize:binSize:glmData.trial(1).duration]; % time bin centres (ms)  
        
        % LOOP all trials 
        for trial = 1:length(trialIdx)-1
            startIdx = trialIdx(trial) + 1; 
            endIdx = trialIdx(trial+1);
            % predicted rate
            predictedRate = exp(dm.X*wml);
            % different coherence levels
            switch glmData.trial(trial).coh
                    case 0
                        cohIdx = 1;
                    case 32
                        cohIdx = 2;
                    case 64
                        cohIdx = 3;
                    case 128
                        cohIdx = 4;
                    case 256
                        cohIdx = 5;
                    case 512
                        cohIdx = 6;                    
            end
            % add trials for in/out-RF
            if glmData.trial(trial).inRF == 1 && glmData.trial(trial).coh ~= 0            
                spikeHist(cohIdx).inRF(end+1,:) = [hist(glmData.trial(trial).sptrain,histCentres)]; % bin spike trains
                modelHist(cohIdx).inRF(end+1,:) = [predictedRate(startIdx:endIdx); zeros(1,length(histCentres)-length(predictedRate(startIdx:endIdx)))'];
            elseif glmData.trial(trial).inRF == -1 && glmData.trial(trial).coh ~= 0   
                spikeHist(cohIdx).outRF(end+1,:) = [hist(glmData.trial(trial).sptrain,histCentres)];
                modelHist(cohIdx).outRF(end+1,:) = [predictedRate(startIdx:endIdx); zeros(1,length(histCentres)-length(predictedRate(startIdx:endIdx)))'];
            elseif glmData.trial(trial).coh == 0 % zero coherence
                spikeHist(cohIdx).inRF(end+1,:) = [hist(glmData.trial(trial).sptrain,histCentres)]; % bin spike trains
                spikeHist(cohIdx).outRF(end+1,:) = [hist(glmData.trial(trial).sptrain,histCentres)];
                modelHist(cohIdx).inRF(end+1,:) = [predictedRate(startIdx:endIdx); zeros(1,length(histCentres)-length(predictedRate(startIdx:endIdx)))'];
                modelHist(cohIdx).outRF(end+1,:) = [predictedRate(startIdx:endIdx); zeros(1,length(histCentres)-length(predictedRate(startIdx:endIdx)))'];
            end
        end
        
        % now separate by coherence: 512, 256, 128, 64, 32, 0
        
        % average
        for cohVal = 1:length(spikeHist)
            % collate by coherence and smooth
            spikeHist_inAvg{cohVal} = smooth_spikes(mean(spikeHist(cohVal).inRF,1),'boxcar',10);
            spikeHist_outAvg{cohVal} = smooth_spikes(mean(spikeHist(cohVal).outRF,1),'boxcar',10);
            modelHist_inAvg{cohVal} = mean(modelHist(cohVal).inRF,1);
            modelHist_outAvg{cohVal} = mean(modelHist(cohVal).outRF,1);
        end
        
        
        
        % plot
        figure
        for cohVal = 1:length(spikeHist)
            subplot(2,length(spikeHist)/2,cohVal); hold on
            bar(histCentres,spikeHist_inAvg{cohVal}/(binSize*1e-3),1.0,'b') % divide actual binned rate by binSize
            bar(histCentres,spikeHist_outAvg{cohVal}/(binSize*1e-3),1.0,'r') % divide actual binned rate by binSize
%             if modelType == 2
%                 stepRate = predictedRate(startIdx:endIdx);
%                 steptime = find(stepRate ~= stepRate(1),1,'first');
%                 plot(histCentres(1:steptime),stepRate(1)*ones(1,length(stepRate(1:steptime))),modelClrs{modelType}) % bottom rate
%                 plot(histCentres(steptime:end),stepRate(steptime)*ones(1,length(stepRate(steptime:end))),modelClrs{modelType}) % top rate
%                 plot([histCentres(steptime) histCentres(steptime)], [stepRate(1) stepRate(steptime)],modelClrs{modelType}); % vertical line
%             else
                % plot model predictions
                plot(histCentres,modelHist_inAvg{cohVal}/(1e-3), 'b')
                plot(histCentres,modelHist_outAvg{cohVal}/(1e-3), 'r')
                if cohVal == 1
                    plot(histCentres,modelHist_inAvg{cohVal}/(1e-3), 'k')
                    bar(histCentres,spikeHist_inAvg{cohVal}/(binSize*1e-3),1.0,'k') % divide actual binned rate by binSize
                end
            % format axes
            ylim([0 70])
            yticks([0 70])
            yticklabels([0 70])
            xlim([0 850]) % histCentres(end)])
            xticks([0 850])
            xlabel('time (ms)', 'FontSize', 18)
            ylabel('spikes (s^{-1})', 'FontSize', 18)     
            title(sprintf('Coherence: %d', cohVal))
        end % plot coherence values
 
end % modelType
    
    
% Fano factor plots 
clear all; clc
work_folder = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat';
% subjects
subjects = ['b_fd';'n_fd'];
subjects = cellstr(subjects);
% output 
inFano = {};
outFano = {};
inFano{6} = [];
outFano{6} = [];

hiFano = {};
hiFanoRawTrains = {};
hiFanoStimon = [];
hiFanoStimoff = [];
hiFanoSacc = [];
hiFanoCoh = [];
hiFanoRF = [];
synthinFano = {};
synthoutFano = {};
synthinFano{6} = [];
synthoutFano{6} = [];

meanCountin = [];
varCountin = [];
meanCountout = [];
varCountout = [];
synthmeanin = [];
synthvarin = [];
synthmeanout = [];
synthvarout = [];

% numTrials 6   2   2   3   6

% subj num 	1	1	2	2	2
% matFile 	10	25	11	19	26
% coh 		3	1	1	1	2	
% RF          out	in/out	in/out	in/out	out
hiFFmatrix = [ 1 2;
               10 26;
               3 2;
               -1 -1]; % interesting to note that almost all no coherence trials and usually in RF hi var??
           
% LOOP folders
cd(work_folder);
for subj_num = 1:length(subjects) 
    cd(subjects{subj_num});
    files = dir;
    matFiles = {files(~[files.isdir]).name}; % remove self and parent dirs
    % LOOP neurons
    for j = 1:length(matFiles)
        load(matFiles{j});
        % reset spike vectors
        clearvars spikeFF synthFF
        spikeFF.inRF = [];
        spikeFF.outRF = [];
        spikeFF(6).inRF = [];
        synthFF.inRF = [];
        synthFF.outRF = [];
        synthFF(6).inRF = [];       
        % LOOP trials    
        for trialNum = 1:length(glmData.trial)
            % different coherence levels
            switch glmData.trial(trialNum).coh
                    case 0
                        cohIdx = 1;
                    case 32
                        cohIdx = 2;
                    case 64
                        cohIdx = 3;
                    case 128
                        cohIdx = 4;
                    case 256
                        cohIdx = 5;
                    case 512
                        cohIdx = 6;                    
            end
            % save spike trains for plotting  hi FF
            % find trials with hi (=>6) FF


            loopIdx = [subj_num, j, cohIdx, glmData.trial(trialNum).inRF];
            if ismember(loopIdx,transpose(hiFFmatrix),'rows')
               % save spike train
               hiFano{end+1} = glmData.trial(trialNum).sptrain;
               hiFanoRawTrains{end+1} = glmData.trial(trialNum).rawspikes;
               hiFanoStimon(end+1) = glmData.trial(trialNum).origstimon;
               hiFanoStimoff(end+1) = glmData.trial(trialNum).origstimoff;
               hiFanoSacc(end+1) = glmData.trial(trialNum).sacctime;
               hiFanoCoh(end+1) = glmData.trial(trialNum).coh;
               hiFanoRF(end+1) = glmData.trial(trialNum).inRF;
               % save coherence and in/outRF
            end
            % add trials for in/out-RF
            numCounts = length(glmData.trial(trialNum).sptrain); % number of spikes 
            % make synthetic trial with same mean spike count
            duration = 1e-3*glmData.stimLengths(trialNum) ; % convert to seconds
            poissonSpikes = poissrnd(numCounts/duration); % draw poisson value using same  
            if glmData.trial(trialNum).inRF == 1 && glmData.trial(trialNum).coh ~= 0
                spikeFF(cohIdx).inRF(end+1,:) = numCounts/duration; 
                synthFF(cohIdx).inRF(end+1,:) = poissonSpikes;
            elseif glmData.trial(trialNum).inRF == -1 && glmData.trial(trialNum).coh ~= 0   
                spikeFF(cohIdx).outRF(end+1,:) = numCounts/duration;
                synthFF(cohIdx).outRF(end+1,:) = poissonSpikes;
            elseif glmData.trial(trialNum).coh == 0 % zero coherence
                spikeFF(cohIdx).inRF(end+1,:) = numCounts/duration; 
                spikeFF(cohIdx).outRF(end+1,:) = numCounts/duration;
                synthFF(cohIdx).inRF(end+1,:) = poissonSpikes;
                synthFF(cohIdx).outRF(end+1,:) = poissonSpikes;
            end
        end %trials
            % calculate FF for each coherence
            for coh = 1:length(spikeFF)
                if length(spikeFF(coh).inRF) > 5
                    meanCountin(end+1) = mean(spikeFF(coh).inRF); % average spike counts across trials for condition
                    varCountin(end+1) = std(spikeFF(coh).inRF)^2; % variance spike counts across trials for condition 
                    syntheticPoisson = poissrnd(meanCountin(end),1,length(spikeFF(coh).inRF));
                    synthmeanin(end+1) = mean(syntheticPoisson);
                    synthvarin(end+1) = std(syntheticPoisson)^2;
                    inFano{coh}(end+1) = (varCountin)/meanCountin;
                    synthinFano{coh}(end+1) = (synthvarin/synthmeanin);
%                     if varCountin(end)/meanCountin(end) >= 6
%                         keyboard
%                     end
                end
                if length(spikeFF(coh).outRF) > 5
                    meanCountout(end+1) = mean(spikeFF(coh).outRF);
                    varCountout(end+1) = std(spikeFF(coh).outRF)^2;
                    outFano{coh}(end+1) = (varCountout)/meanCountout;
                    syntheticPoisson = poissrnd(meanCountout(end),1,length(spikeFF(coh).outRF));
                    synthmeanout(end+1) = mean(syntheticPoisson);
                    synthvarout(end+1) = std(syntheticPoisson)^2;
                    synthoutFano{coh}(end+1) = (synthvarout/synthmeanout);
%                     if varCountout(end)/meanCountout(end) >= 6
%                         keyboard
%                     end
                end
            end 
      
    end % neurons
    cd(work_folder);
end




for trials = 1:length(inFano)
    if inFano{trials} >= 6
        trials
    end
end

% plot inRF
figure; hold on
for idx = 1:length(inFano)
    row = idx*ones(1,length(inFano{idx})) + 0.1*rand(1,length(inFano{idx}));
    scatter(row, inFano{idx},'.b')
    scatter(idx,mean(inFano{idx}),'.k')
    abc = find(inFano{idx} < 1);
%     length(inFano{idx})
%     length(abc)
end
title('real spikes inRF')
xticklabels({'0','32','64','128','256','512'})
ylabel('Fano Factor')
xlim([0 6.5])
xlabel('Coherence')

% plot outRF
figure; hold on
for idx = 1:length(outFano)
    row = idx*ones(1,length(outFano{idx})) + 0.1*rand(1,length(outFano{idx}));
    scatter(row, outFano{idx},'.r')
    scatter(idx,mean(outFano{idx}),'.k')
end
title('real spikes outRF')
xticklabels({'0','32','64','128','256','512'})
ylabel('Fano Factor')
xlim([0 6.5])
xlabel('Coherence')

% plot synthetic inRF
figure; hold on
for idx = 1:length(synthinFano)
    row = idx*ones(1,length(synthinFano{idx})) + 0.1*rand(1,length(synthinFano{idx}));
    scatter(row, synthinFano{idx},'.b')
    scatter(idx,mean(synthinFano{idx}),'.k')
end
title('synthetic inRF')
xticklabels({'0','32','64','128','256','512'})
ylabel('Fano Factor')
xlim([0 6.5])
xlabel('Coherence')

% plot synthetic in/outRF
figure; hold on
for idx = 1:length(synthoutFano)
    row = idx*ones(1,length(synthoutFano{idx})) + 0.1*rand(1,length(synthoutFano{idx}));
    scatter(row, synthoutFano{idx},'.r')
    scatter(idx,mean(synthoutFano{idx}),'.k')
end
title('synthetic outRF')
xticklabels({'0','32','64','128','256','512'})
ylabel('Fano Factor')
xlim([0 6.5])
xlabel('Coherence')

% scatter mu vs variance
figure; hold on
scatter(meanCountin,varCountin,'.b')
scatter(meanCountout,varCountout,'.r')
scatter(synthmeanin,synthvarin,30,'+')
scatter(synthmeanout,synthvarout,30,'+')
x = 0:1000;
y = x;
plot(x,y,'k')
xlim([0, 100])
xlabel('mean (Hz)')
ylabel('variance')


% histogram of FF 
% allsynthoutFano = [];
% allsynthinFano = [];
% allinFano = [];
% alloutFano = [];
% 
% 
% 
% 
% for idx = 1:length(synthoutFano)
%     allsynthoutFano = [allsynthoutFano synthoutFano{idx}];
%     allsynthinFano = [allsynthinFano synthinFano{idx}];
%     allinFano = [allinFano inFano{idx}];
%     alloutFano = [alloutFano outFano{idx}];
% end


% histogram of FF with example trains of hi FF
binCentres = [0:0.5:20];
barWidth = 1.0;
% histogram(varCountin./meanCountin,binCentres,'facecolor','r','FaceAlpha',0.3,'edgecolor','none')
% histogram(varCountout./meanCountout,binCentres,'facecolor','b','facealpha',.3,'edgecolor','none')
% histogram(synthvarin./synthmeanin,binCentres,'facecolor','g','facealpha',.3,'edgecolor','none')
% histogram(synthvarout./synthmeanout,binCentres,'facecolor','y','facealpha',.3,'edgecolor','none')
h1 = histcounts(varCountin./meanCountin,binCentres); % actually edges


h2 = histcounts(varCountout./meanCountout,binCentres);
h3 = histcounts(synthvarin./synthmeanin,binCentres);
h4 = histcounts(synthvarout./synthmeanout,binCentres);



figure; hold on 
inRF_idx = find(hiFanoRF == 1);
outRF_idx = find(hiFanoRF == -1);
bothRF_idx = [inRF_idx, outRF_idx];
for sptrain = 1:length(hiFano)
    subplot(length(hiFano),2,sptrain*2-1)
    if hiFanoRF(bothRF_idx(sptrain)) == 1
        color = 'b';
    elseif hiFanoRF(bothRF_idx(sptrain)) == -1
        color = 'r';
    end
    rasterplt(hiFanoRawTrains{bothRF_idx(sptrain)},color) 
    hold on
      
    stimon = hiFanoStimon(bothRF_idx(sptrain));
    stimoff = hiFanoStimoff(bothRF_idx(sptrain));
    sacctime = hiFanoSacc(bothRF_idx(sptrain));
    plot([stimon;stimon],[ones(size(stimon));zeros(size(stimon))],'k-','LineWidth',2)
%     plot([stimoff;stimoff],[ones(size(stimoff));zeros(size(stimoff))],'k-')
    plot([sacctime;sacctime],[ones(size(sacctime));zeros(size(sacctime))],'g-','LineWidth',2)
    xlim([-200+stimon stimon+3000])
    set(gca,'XTick', []) % don't draw x-axis ticks
end  

set(gca,'XTick',[stimon stimon+3000],'FontSize',16)  
set(gca,'XTickLabels',{'0', '3000'},'FontSize',16)  
xlabel('ms', 'FontSize', 20)
% plot histogram of FF distribution
hist_ax = subplot(length(hiFano),2,2);
clr = [0 0 1;
       1 0 0;
       0 0 0;
      0 0 0];
colormap(hist_ax,clr); % automatically calls figure
hold on
h = bar(binCentres(1:end-1),[h1; h2; h3; h4]','BarWidth', barWidth);
h(3).FaceAlpha = 0.5; % synthetic inRF
h(4).FaceAlpha = 0.2; % synthetic outRF
xlabel('Fano Factor','FontSize',20)
set(gca, 'FontSize', 16)
ylim([0 21])
xlim([-1 10])
xticks([0:9])
yticks([10, 20])
% hiFanoCoh

% TODO: >5 trials per condition; black lines, thickness x2, 3 groups of
% conditions separate by black lines
% TODO: fix 3rd row!!!
% TODO: align to stim on; make lines thicker
