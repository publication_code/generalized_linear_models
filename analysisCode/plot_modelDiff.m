function [mainplth,h] = plot_modelDiff(neurons_ll,model1Mean,model2Mean,model1Var,model2Var,barclr,pdf_shade,mainplth,h)

numNeurons = length(neurons_ll);

% plot mean differences across neurons d
axes(mainplth); hold on %set the current axes to axes2
numTrialsVec = [];
for j = 1:numNeurons
    numTrialsVec(end+1) = neurons_ll{j}.nTrials;
end

errorbar(model1Mean-model2Mean,sqrt( (model1Var+model2Var)./(10*numTrialsVec) ),strcat(barclr,'.')) % sem; 10 CV folds multiply by numTrials
plot([0:numNeurons+1],zeros(numNeurons+2),'k'); % horizontal line at ll = 0

% for const+hx - const
keyboard
meandiff = sum(model1Mean - model2Mean)/numNeurons; % mean diff across neurons
sem = sqrt( var(model1Mean-model2Mean) / numNeurons );


% format axes
yaxisHeight = max(abs(model1Mean-model2Mean));
ylim([-5 11])
% xticks([1 length(model1Mean)])
% yticks([-5 0 5 10])
xlabel('neuron id', 'FontSize', 18)
% xticklabels({'-3\pi','-2\pi','-\pi','0','\pi','2\pi','3\pi'})
% yticks([-1 -0.8 -0.2 0 0.2 0.8 1])

% distribution of means 
[counts binCenters] = hist(model1Mean-model2Mean, 10); 
% create miniplot within main plot 
p = get(gca, 'Position'); % Find the position of the current axes and then create a small axes using that position data.


allAxes = findall(gcf,'type','axes'); % miniplot is 2nd set of axes TODO: only if axes is odd; otherwise use last set! 
if isempty(h) % if not odd then use existing miniplot handle
    h = axes('Parent', gcf, 'Position', [p(3)*.9+p(1) p(2) p(2)*0.3 p(4)]); % [left bottom width height] wrt to whole figure 
    % rotate axes
    camroll(90)
    % set xlim same as parent ylim
    xlim([-5 10])
    set(h, 'YTick', [], 'YTickLabels', []);
    set(h, 'XTick', [], 'XTickLabels', []);
    % set transparent
    set(gca,'color','none')
end

% set(gca, 'Position', [hsp2(1:3)  2*hsp1(4)])    % Use 2*(2,1,1) Height for (2,1,2)

axes(h); hold on
area(binCenters, counts/sum(counts),'FaceColor',pdf_shade,'FaceAlpha',0.3,'EdgeColor','none');
% set(h, 'Xlim', [-0.2 0], 'Ylim', [-5 -4]);
set(h, 'YTick', []);
ylabel('density', 'FontSize', 18)
% remove bounding box
set(h,'box','off')


