for file = 1:length(rawspikes)
    trial=rawspikes{file}{1}
    trialDuration = [trialDuration trial(end)];
end

dtStim = 10; % ms; bin size
ncells = length(spTrains);  % number of neurons (10 for this dataset).
nT = ceil(max(trialDuration)/dtStim); % number of time bins in stimulus
tbins = (.5:nT)*dtStim; % time bin centers for spike train binnning
sps = zeros(nT,ncells);

% bin spikes 
for jj = 1:ncells
    sps(:,jj) = hist(rawspikes{jj}{1},tbins)';  % binned spike train; bins x cells
end

% Pick the cell to focus on
cellnum = 3;
% Set the number of time bins of stimulus to use for predicting spikes
ntfilt = 25;  % Try varying this, to see how performance changes!
% Set number of time bins of auto-regressive spike-history to use
nthist = 20;

% Build stimulus design matrix (using 'hankel');
paddedStim = [zeros(ntfilt-1,1); stimTrains(:,cellnum)]; % pad early bins of stimulus with zero
Xstim = hankel(paddedStim(1:end-ntfilt+1), stimTrains(end-ntfilt+1:end,cellnum));



%% Basis functions for spike hx filter

% TODO: do we actually need this; not doing encoding

% restructure stimulus trains
for column = 1:length(stimCoh)
    stimBins = ceil(trialDuration(column)/dtStim);
    stimTrains(:,column) =  [stimCoh(column)*ones(stimBins,1); ones(nT-stimBins, 1)];
end

%% Regularization

%% Model comparison: log-likelihoood and AIC
