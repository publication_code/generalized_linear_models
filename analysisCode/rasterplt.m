function rasterplt(in,varargin)
if size(in,1) > size(in,2)
    in=in';
end
axis([0 max(in)+1 -1 2])
if nargin == 1
    plot([in;in],[ones(size(in));zeros(size(in))],'k-')
else
   plot([in;in],[ones(size(in));zeros(size(in))],varargin{1}) 
end
set(gca,'TickDir','out') % draw the tick marks on the outside
set(gca,'YTick', []) % don't draw y-axis ticks
set(gca,'PlotBoxAspectRatio',[1.0 0.05 1.0]) % short and wide [x,y(height),zdir(does nothing)]
set(gca,'YColor',get(gcf,'Color')) % hide the y axis
% set(gca,'Color',get(gcf,'Color')) % match figure background

box off
