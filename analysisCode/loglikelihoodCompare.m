% compare -log-likelihoods of step and ramp models 
clear all; clc
trialType = {'fixed duration'; 'reaction time'};
fd_subjects = ['b_fd';'n_fd']; 
rt_subjects = ['b_rt';'n_rt'];
fd_subjects = cellstr(fd_subjects);
rt_subjects = cellstr(rt_subjects);
analyze_timebin = '10_timebin'; 
models = {'constant' 'ramp' 'step'}; % for stimonlyGLMresults
histType = {'nohist','withhist'};
cv_folds = 10; 
% load data
addpath('~/Documents/Analysis/stepramp_comparison/RoitmanDataCode/')
origpath = '~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/';
savepath = strcat(origpath,'stimonlyGLMresults/plot_data/');
for histcov = histType
    datapath = strcat('~/Documents/Analysis/stepramp_comparison/RoitmanData_GLMformat/stimonlyGLMresults/',char(histcov),'/');
    for t = 1:length(trialType)
        if strcmp(trialType{t},'fixed duration')
            subjects = fd_subjects;
        elseif strcmp(trialType{t},'reaction time')
            subjects = rt_subjects;
        end
    for modelType = 1:length(models)
        display(sprintf('Analyzing %s and model type: %s', analyze_timebin,models{modelType}))
        neurons_ll = {}; % reset LL for neurons
        neuronCounter = 1;
        for subj = 1:length(subjects)
            cd(strcat(origpath,(subjects{subj})));   
            origFiles = dir;
            origmatFiles = {origFiles(~[origFiles.isdir]).name};
            cd(strcat(datapath,analyze_timebin,'/',models{modelType},'/',(subjects{subj})));   
            files = dir;
            matFiles = {files(~[files.isdir]).name};
            for i = 1:length(matFiles) % LOOP mat files
                try
                    load(matFiles{i})
                catch e
                    continue
                end
                matchStr = matFiles{i}(1:5);
                remainingFiles = matFiles(~cellfun('isempty',matFiles));  
                matchFiles = strfind(remainingFiles,matchStr); % finds all files that match current neuron    
                fileIdx = find(~cellfun(@isempty,matchFiles));
                cvFiles = remainingFiles(fileIdx);
                logLik = []; EM = [];
                for j = 1:length(cvFiles)
                    load(cvFiles{j})
                    logLik(end+1) = LLtest;
                    EM(end+1) = EMepsilon; 
                    % delete this matFile from list
                    index = find(strcmp(matFiles,cvFiles{j}));
                    matFiles{index} = [];
                end
                neurons_ll{neuronCounter}.logLik = logLik;
                neurons_ll{neuronCounter}.EM = EM; 
                neurons_ll{neuronCounter}.name = matchStr;
                cd(strcat(origpath,(subjects{subj})));   
                orig_GLMfile = strfind(origmatFiles,matchStr); % finds all files that match current neuron  
                origIdx = find(~cellfun(@isempty,orig_GLMfile));
                load(origmatFiles{origIdx});
                neurons_ll{neuronCounter}.nTrials = glmData.nTrials;
                neurons_ll{neuronCounter}.dprime = glmData.dprime; 
                neuronCounter = neuronCounter + 1;
                cd(strcat(datapath,analyze_timebin,'/',models{modelType},'/',(subjects{subj}))); % change back into matFile folder
            end
        end % subjects

        % calculate means and variances for each neuron 
        switch models{modelType}
            case 'ramp'
                [rampMean,rampVar,rampEM] = modelStats(neurons_ll);          
            case 'step'
                [stepMean,stepVar,stepEM] = modelStats(neurons_ll); 
            case 'constant'
                [constMean,constVar,constEM] = modelStats(neurons_ll); 
        end   
    end % models

    % save 
    task = trialType{t};
    save(strcat(savepath,task,'_',char(histcov)),'stepMean','rampMean','constMean','stepVar','rampVar','constVar','rampEM','stepEM','constEM','neurons_ll')
    % cd to working folder
    end % trialType
end % history covariate

% hyp = zeros(1,length(matFiles)); % hypothesis: 1- different, 0- null; for each neuron
% pVal = zeros(1,length(matFiles));
% ci = zeros(1,length(matFiles));
keyboard




        
        
        
%     suptitle(sprintf('Model comparison for %sms timebins', analyze_timebin(1:end-8),subjects{subj}(end-1:end),subjects{subj}(1)))
% t-test
% 2 sample t-test: are means of 2 diff distributions the same
% un-paired (ttest2): both distributions have no relation
% null hypothesis: x,y independent random samples with normal distbn equal means, unknown var; 1= reject 5%
% for k = 1:length(stepMean) % LOOP all mat files 
%     [hyp(k),pVal(k),ci(k,:),stats] = ttest2(rampMean, stepMean); 
% end
% F-statistic; ratio of the mean sq. errors 
    
  
% TODO:
% bootstrap
% ci = bootci(999,@mean,rampMean-stepMean); % 95 CI
% plot means and variances (excl. outliers from ramp results)

% TODO: GLM regularizer? elastic net?  
% TODO: in/out receptive field? coherence? 

