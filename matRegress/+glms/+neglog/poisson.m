function [L,dL,H] = poisson(wts,x,y,fnlin,inds)
% negative log-likelihood of data under Poisson model
% [L,dL,ddL] = neglogli.poisson(wts,X,Y,fnlin,inds)
% w, dm.X, y, fnlin 
% Compute negative log-likelihood of data under Poisson regression model,
% plus gradient and Hessian
%
% INPUT:
% wts [m x 1] - regression weights
%   X [N x m] - regressors
%   Y [N x 1] - output (binary vector of 1s and 0s).
%       fnlin - func handle for nonlinearity (must return f, df and ddf)
%	 inds - (optional) indices to evaluate on subst of X and Y
%
% OUTPUT:
%   L [1 x 1] - negative log-likelihood
%  dL [m x 1] - gradient
% ddL [m x m] - Hessian

if nargin > 4
    x = x(inds, :);
    y = y(inds);
end

xproj = x*wts; % time bins, bases * weights -> time bins 
dL = 0;
H = 0;

switch nargout
    case 1
        f = fnlin(xproj);

	nzidx = f ~= 0;
	if any(y(~nzidx) ~= 0)
	    L = Inf; % if rate is 0, nothing else can happen
	else
	    L = -y(nzidx)'*log(f(nzidx)) + sum(f); % neg log-likelihood
	end
    case 2
        [f,df] = fnlin(xproj); % evaluate nonlinearity; 

	nzidx = f ~= 0;
	if any(y(~nzidx) ~= 0)
	    L = Inf; % if rate is 0, nothing else can happen
	else
	    L = -y(nzidx)'*log(f(nzidx)) + sum(f); % neg log-likelihood
	end
        
        dL = x(nzidx, :)' * ((1 - y(nzidx)./f(nzidx)) .* df(nzidx));
    case 3
        [f,df,ddf] = fnlin(xproj); % evaluate nonlinearity; exp(x*wts); exp(time bins after weighting)
% keyboard
	nzidx = f ~= 0; % indices where the function is nonzero
	if any(y(~nzidx) ~= 0) % y vector of events
	    L = Inf; % if rate is 0, nothing else can happen
	else
	    L = -y(nzidx)'*log(f(nzidx)) + sum(f); % neg log-likelihood; y is vector of time bins when spike occurs; l(lambda;x) = sum(xi*log(lambda)) - n*lambda; first term is summation
    end
        yf = y(nzidx) ./ f(nzidx); % divide spike events by nonlinearity
        dL = x(nzidx, :)' * ((1 - yf) .* df(nzidx)); % grad; x is derivative of (x*wts) and dot prod = summation 
        %H = bsxfun(@times,ddf.*(1-yf)+df.*(y./f.^2.*df) ,x)'*x;
        H = bsxfun(@times, ddf(nzidx) .* (1-yf) ...
	    + (y(nzidx).*(df(nzidx)./f(nzidx)).^2), x(nzidx, :))'* x(nzidx,:); % Hessian; bsxfun does element-wise multiplication with singleton expansion
end

if isnan(L) || any(isnan(dL)) || any(isnan(H(:)))
    warning('glms:neglog:poisson:nan', 'NaN in negative log-likelihood');
end

L = full(L); dL = full(dL); H = full(H); % conver to full matrix
