function [L,dL,ddL] = neglogli_poiss(prs,X,Y,nlfun,dtbin)
% [L,dL,ddL] = neglogli_poiss(prs,X,Y,g,dtbin)
% from mtlipGLM ver. of neuroGLM
% Compute negative log-likelihood of data Y given rate g(X*prs).
%
% INPUT:
%       prs [M x 1] - parameters 
%         X [N x M] - design matrix
%         Y [N x 1] - observed Poisson random variables
%             nlfun - handle for transfer function
%     dtbin [1 x 1] - size of time bins of Y 
%
% OUTPUT: 
%         L [1 x 1] - negative log-likelihood
%        dL [M x 1] - gradient
%       ddL [M x M] - Hessian (2nd deriv matrix)
%
% written: 7 Apr 2012 (JW Pillow)

% Project parameters onto design matrix
z = X*prs;
etol = 1e-100;
if nargout==1
    % Compute neglogli
    f = nlfun(z)*dtbin;
    f(f<etol)=etol;
    L = -Y'*log(f) + sum(f); % L(theta) = sum(log{lambda[t]}) - dt*sum(lambda[t]) + k
elseif nargout == 2
    % Compute neglogli & Gradient
    [f,df] = nlfun(z);
    f = f.*dtbin; df = df.*dtbin;
    f(f<etol)=etol;
    L = -Y'*log(f) + sum(f); 
    % grad
    wts = (df-(Y.*df./f));  % dL/dlambda; lambda = coefficients of dsgn matrix
    dL = X'*wts;            % d(X*w)/dw = X'
elseif nargout == 3
    % Compute neglogli, Gradient & Hessian
    [f,df,ddf] = nlfun(z);
    f=f.*dtbin;df=df.*dtbin;ddf=ddf.*dtbin;
    f(f<etol)=etol;
    L = -Y'*log(f) + sum(f);
    % grad
    wts = (df-(Y.*df./f));
    dL = X'*wts;
    ww = ddf-Y.*(ddf./f-(df./f).^2); % d^2L/dlambda^2
    ddL = X'*bsxfun(@times,X,ww); % elem-wise op to X, ww
end
