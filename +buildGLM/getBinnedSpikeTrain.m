function [y,endTrialIndices] = getBinnedSpikeTrain(expt, spLabel, trialIdx)
% y: a sparse column vector representing the concatenated spike trains
sts = cell(numel(trialIdx), 1);
binfun = expt.binfun;
endTrialIndices = [0 cumsum(binfun([expt.trial(trialIdx).duration]))]; % cumulative sum of bins across all trials; gives bin # for end of each trial
nT = endTrialIndices(end); % how many bins across all trials

for kTrial = trialIdx(:)'
    bst = endTrialIndices(kTrial) + binfun(expt.trial(kTrial).(spLabel)); % spike train for kth trial with cumulative indexing
    sts{kTrial} = bst(:);
end

sts = cell2mat(sts);
y = sparse(sts(sts <= nT), 1, 1, nT, 1); % generate nT x 1 sparse matrix; bins with spike count
