function glmData = doubleUpdate(glmData,binSize,vecT0,vecR0)
% add t0,r0 fields for integration models step/ramp  
if isfield(glmData.trial,'t0')
    % do nothing
else 
    glmData.trial(1).t0 = []; % creates empty cell for all trials
end

if isfield(glmData.trial,'r0')
    % do nothing
else 
    glmData.trial(1).r0 = []; % creates empty cell for all trials
end

% add r0,t0 field to each trial of glmData
for i = 1:glmData.nTrials
    glmData.trial(i).r0 = vecR0(i); % firing rate
    glmData.trial(i).t0 = binSize*(round(glmData.trial(i).dotson/binSize) + vecT0(i)); % convert from numBins -> ms for trial design matrix
end 

