function glmData = updateSteptimes(glmData,binSize,vect0,modelType)
% add t0 for model step/ramp times 
if isfield(glmData.trial,'t0')
    % do nothing
else 
    glmData.trial(1).t0 = []; % creates empty cell for all trials
end

if strcmp(modelType,'constant') % t0 field now becomes constant firing rate
    for i = 1:glmData.nTrials
        glmData.trial(i).t0 = vect0(i); % convert back to ms for trial design matrix
    end 
else 
    for i = 1:glmData.nTrials
        glmData.trial(i).t0 = binSize*(round(glmData.trial(i).dotson/binSize) + vect0(i)); % convert back to ms for trial design matrix
    end
end
