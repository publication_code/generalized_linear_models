function dm = compileSparseDesignMatrix(dspec, trialIndices)
% Compile information from experiment according to given DesignSpec
expt = dspec.expt;

totalT = sum(ceil([expt.trial(trialIndices).duration]/expt.binSize)); % total number of bins across all trials
if ~isempty(fieldnames(dspec.covar))
    subIdxs = buildGLM.getGroupIndicesFromDesignSpec(dspec); % beginning indices of each covariate in design matrix as cell array


    growingX = sparse([], [], [], 0, dspec.edim, round(totalT * dspec.edim * 0.001)); % preallocate

    trialIndices = trialIndices(:)';

    for kTrial = trialIndices % LOOP all trials
        nT = ceil(expt.trial(kTrial).duration / expt.binSize); % bins in current trial

        miniX = zeros(nT, dspec.edim); % pre-allocate a dense matrix for each trial; bins x total basis vecs

        for kCov = 1:numel(dspec.covar) % LOOP covariates 
            covar = dspec.covar(kCov);
            sidx = subIdxs{kCov};

            if isfield(covar, 'cond') && ~isempty(covar.cond) && ~covar.cond(expt.trial(kTrial)) % skip covariate
                continue;
            end
            try
            stim = covar.stim(expt.trial(kTrial), nT); % dense or sparse repx of covariate ts; stimhandle(trial.covariate, bins); (label) refers to covar field
            catch MExc
            end
            if isfield(covar, 'basis') && ~isempty(covar.basis)
                miniX(:, sidx) = basisFactory.convBasis(stim, covar.basis, covar.offset); % convolve basis functions to the covariate matrix
            else
                miniX(:, sidx) = stim; % covariate ts with no basis convolution
            end
        end
        growingX = [growingX; sparse(miniX)]; % append covariates for current trial vertically
    end
    dm.X = growingX;
    
else
    dm.X = zeros(1,totalT)';
end
dm.trialIndices = trialIndices;
dm.dspec = dspec;

%% Check sanity of the design
if any(~isfinite(dm.X(:)))
    warning('Design matrix contains NaN or Inf...this is not good!');
end

% constant column on the design matrix
dm = buildGLM.removeConstantCols(dm); % redundant if adding bias column
dm = buildGLM.addBiasColumn(dm); 
