function glmData = updateModelSigns(glmData,signVec)
% add t0 for model step/ramp times 
if isfield(glmData.trial,'modelsign')
    % do nothing
else 
    glmData.trial(1).modelsign = []; % creates empty cell for all trials
end

for i = 1:glmData.nTrials
    glmData.trial(i).modelsign = signVec(i); % convert back to ms for trial design matrix
end
