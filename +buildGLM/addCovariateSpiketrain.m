function dspec = addCovariateSpiketrain(dspec, covLabel, stimLabel, desc, basisStruct, varargin)

if nargin < 4 || isempty(desc); desc = covLabel; end

if nargin < 5
    basisStruct = basisFactory.makeNonlinearRaisedCos(8, dspec.expt.binSize, [0 200], 2); % nBases, binSize, 1st/last peak, nlOffset
end

assert(ischar(desc), 'Description must be a string');
offset = 1; % Make sure to be causal. No instantaneous interaction allowed.
binfun = dspec.expt.binfun;
stimHandle = @(trial, expt) basisFactory.deltaStim(binfun(trial.(stimLabel)), binfun(trial.duration)); 
% Returns a sparse vector with events at binned timings
dspec = buildGLM.addCovariate(dspec, covLabel, desc, stimHandle, basisStruct, offset, varargin{:});
% offset- number of time bins to shift regressors

% Huk, 2017 used 50 offset (nlScale) and incorporated refractory period...
% meh.
