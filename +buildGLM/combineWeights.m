function [wout] = combineWeights(dm, w)
% Combine the weights per column in the design matrix per covariate
%
% Input
%   dm: design matrix structure
%   w: weights (from ML) on the basis functions
%
% Output
%   wout.(label).data = combined weights
%   wout.(label).tr = time axis
% returns weights multiplied with basis for each covariate

dspec = dm.dspec;
binSize = dspec.expt.binSize;

if isfield(dm, 'biasCol') % undo bias column
    wout.bias = w(dm.biasCol); % weight for bias column from 'w'
    w(dm.biasCol) = []; % delete weighting for bias column
end

if isfield(dm, 'zscore') % undo z-score operation
    w = (w .* dm.zscore.sigma(:)) + dm.zscore.mu(:);
end

if isfield(dm, 'constCols') % put back the constant columns
    w2 = zeros(dm.dspec.edim, 1);
    w2(~dm.constCols) = w; % first term is bias; remove it
    w = w2;
end

if numel(w) ~= dm.dspec.edim
    error('Expecting w to be %d dimension but it''s [%d]', ...
	dspec.edim, numel(w));
end

startIdx = [1 (cumsum([dspec.covar(:).edim]) + 1)]; % Indices of bases relevant for each covariate
wout = struct(); % deletes previous bias col? 

for kCov = 1:numel(dspec.covar)
    covar = dspec.covar(kCov);
    basis = covar.basis;

    if isempty(basis)
	w_sub = w(startIdx(kCov) + (1:covar.edim) - 1); % basis weights relevant to current covariate
	wout.(covar.label).tr = ((1:size(w_sub, 1))-1 + covar.offset) * binSize; % time indices
	wout.(covar.label).data = w_sub;
	continue;
    end

    assert(isstruct(basis), 'Basis structure is not a structure?');

    sdim = covar.edim / basis.edim; % dims / covariate = stimulus dimension 
    wout.(covar.label).data = zeros(size(basis.B, 1), sdim); % time bins, timeseries data for covar
    for sIdx = 1:sdim % # stimuli for current covariate
        w_sub = w(startIdx(kCov) + (1:basis.edim)-1 + basis.edim * (sIdx - 1)); % the set of edims for each covariate, iterate through timeseries of covariate
        w2_sub = sum(bsxfun(@times, basis.B, w_sub(:)'), 2); % matrix multiply basis.B by w_sub -> 
        wout.(covar.label).data(:, sIdx) = w2_sub;
    end
    wout.(covar.label).tr = (basis.tr(:, 1) + covar.offset * binSize) * ones(1, sdim); % time columns for sdim (stimulus dimension)
end
