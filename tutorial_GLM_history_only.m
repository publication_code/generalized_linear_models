%% Simulate a simple GLM with history filter (no other covariates)
unitOfTime = 'ms';
binSize = 1; % 1 ms
T = 100000; % number of time bins
rateBias = log(22/1000); % 22 Hz; -3.8 what is this? +ve rate bias?

% make some history filter
w = fliplr([0.01 0.03 0.1 0.2 0.35 0.3 0.1 0 -0.05 -0.1 -0.2 -0.3 -0.5 -0.6 -1 -3 -3.5 -4 -4 -4 -6]); % flip left to right
nHistBins = numel(w);
y = zeros(T + nHistBins, 1);

for t = (nHistBins+1):(T+nHistBins) % for leave out first nHistBins, repeat for T
    yy = poissrnd(exp(w * y(t - (1:nHistBins)) + rateBias));
    if yy ~= 0
	y(t) = 1;
    end
end

y = y(nHistBins+1:end);
st = find(y); % nonzero indices of y; spike times

fprintf('Mean rate: %f Hz\n', mean(y) * 1e3); % mean(y) average spike rate / ms bin
figure(933); clf
hist(diff(st), 0:201); xlim([0 200]);
title('Interspike-interval (ISI) distribution');

%% Fit a model
expt = buildGLM.initExperiment(unitOfTime, binSize);
expt = buildGLM.registerSpikeTrain(expt, 'sptrain', 'simulated neuron');

%% There's just one trial
expt.trial(1).sptrain = st; % spike times
expt.trial(1).duration = T; % number of bins in trial

dspec = buildGLM.initDesignSpec(expt); % add expt as a field, covar, idxmap: reverse positional indexing
%dspec = buildGLM.addCovariateSpiketrain(dspec, 'hist', 'sptrain', 'History filter');
bs = basisFactory.makeSmoothTemporalBasis('boxcar', 24, 12, expt.binfun); % 24- duration, 12- # bases
dspec = buildGLM.addCovariateSpiketrain(dspec, 'hist', 'sptrain', 'History filter', bs); % if no basis given then default is log raised cosines
dm = buildGLM.compileSparseDesignMatrix(dspec, 1); % design specs, trial indices
dm = buildGLM.addBiasColumn(dm); % design matrix with 1st column bias (1)

%% Do the regression
addpath('matRegress')

wInit = dm.X \ y; % linear least sq X*w = y; TODO: what algo used? 
fnlin = @nlfuns.exp; % inverse link function (a.k.a. nonlinearity); returns f, df, ddf
lfunc = @(w)(glms.neglog.poisson(w, dm.X, y, fnlin)); % cost function; Compute negative log-likelihood of data under Poisson regression model

opts = optimoptions(@fminunc, 'Algorithm', 'trust-region', ...
    'GradObj', 'on', 'Hessian','on');

[wML, nlogli, exitflag, ostruct, grad, hessian] = fminunc(lfunc, wInit, opts); % value(max-likelihood),lfunc(wInit)=neg log-like, output info,gradient,hessian of lfunc(wInit)
% finds minimum but used -LL; when -LL used -H is returned
wvar = diag(inv(hessian)); % FisherInfo = FI; -H = obsFI(ML); inv(-H) = est. asympt. covar matrix -> sqrt(diag) est. SE

ws = buildGLM.combineWeights(dm, wML); % combine weights across all stimuli, covariates

%% Alternative maximum likelihood Poisson estimation using glmfit
dm = buildGLM.removeConstantCols(dm);
[wglm, devglm, statsglm] = glmfit(dm.X, y, 'poisson', 'link', 'log');
wvarglm = statsglm.se.^2;

%% Plot results
figure(140242); clf; hold all;
plot(w); % actual history filter
plot(ws.hist.data) % predicted history filter
title('History filter');
legend('true', 'estimated', 'Location', 'SouthEast');
xlabel(['Time (' unitOfTime ')']);
