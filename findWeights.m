function [wml,dm,nlogli,exitflag] = findWeights(dm,y,rho,rhoNull,binSize)

% Least squares for initialization
wInit = dm.X \ y; % least sq soln of dm.X * wInit = y 

% Use matRegress for Poisson regression
iirdge = size(dm.X,2); % indices to apply ridge prior to
fnlin = @nlfuns.exp; % inverse link function returns [f,df,ddf]                
mstruct.liargs = {dm.X,y,fnlin,binSize};
mstruct.priargs = {}; % iirdge,rhoNull};
mstruct.neglogli = @glms.neglog.neglogli_poiss;
mstruct.logprior = @glms.neglog.logprior_ridge; 
lfunc = @(w)(glms.neglog.neglogpost_GLM(w,rho,mstruct));


opts = optimoptions(@fminunc, 'Algorithm', 'trust-region', 'GradObj', 'on', 'Hessian','on'); % create optimization opts for specific solver
wInitfull = full(wInit);
[wml, nlogli, exitflag, ostruct, grad, hessian] = fminunc(lfunc, wInitfull, opts);               % find local minimum; lfunc = loss function to minimize; wInit = initial pt; opts = options
% wml = x (soln); nlogli = value of lfunc(x); exitflag = 1 |grad| small, enough, 2 change in X too small, 3 change in obj fxn too small, 0 too many fxn iterations
% ostruct = struct # iterations, function evals, algo used, # CG iterations,  first order optimality, exit msg
% grad = gradient(x); hessian = hessian(x)