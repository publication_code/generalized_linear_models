function [dm, y, endTrialIndices] = build_dsgnMat(unitOfTime,binSize,glmData,modelType,varargin)
% varargin contains flags for including history and integrator covariates

% build design matrix 
expt = buildGLM.initExperiment(unitOfTime, binSize, [], glmData.param);
expt.trial = glmData.trial;
expt = buildGLM.registerTiming(expt, 't0', 'Integration model'); % t0 value for ramp/step models
expt = buildGLM.registerTiming(expt, 'dotson', 'Motion Dots Onset'); % saccade time used as dotsoff
expt = buildGLM.registerTiming(expt, 'dotsoff', 'Motion Dots Offset');
expt = buildGLM.registerTiming(expt, 'targon', 'Targets Turned On');
expt = buildGLM.registerTiming(expt, 'fixon', 'Fixation Point On');
expt = buildGLM.registerTiming(expt, 'fixoff', 'Fixation Point Off'); 
expt = buildGLM.registerTiming(expt, 'saccade', 'Saccade Timing'); % trgac used as saccade time
expt = buildGLM.registerValue(expt, 'coh', 'Coherence'); % information on the trial, but not associated with time
expt = buildGLM.registerValue(expt, 'choice', 'Direction of Choice');
expt = buildGLM.registerSpikeTrain(expt, 'sptrain', 'My Neuron'); 

% Build 'designSpec' which specifies how to generate the design matrix
dspec = buildGLM.initDesignSpec(expt);
binfun = expt.binfun;

if isempty(varargin{1})
    % do nothing
elseif varargin{1} == 'history'
    % Spike history
    dspec = buildGLM.addCovariateSpiketrain(dspec, 'hist', 'sptrain', 'History filter'); % [0 200] ms, 2 nlOffset, 8 bases
end

% % Acausal saccade 
% bs = basisFactory.makeSmoothTemporalBasis('raised cosine', 2500, 10, binfun); % 2500ms
% offset = -ceil(2500/binSize); % number of timebins to shift regressor; negative (+ve) represent acausal (causal) effects
% dspec = buildGLM.addCovariateTiming(dspec, 'saccade1', 'saccade', 'Sac T1', bs, offset, @(trial) (trial.choice == 1));
% dspec = buildGLM.addCovariateTiming(dspec, 'saccade2', 'saccade', 'Sac T2', bs, offset, @(trial) (trial.choice == 2));

% % fixation on
% bs = basisFactory.makeSmoothTemporalBasis('raised cosine', 2000, 10, binfun); % 2000ms
% offset = 0;
% dspec = buildGLM.addCovariateTiming(dspec, 'dotson', [], [], bs, offset);

% % targets on
% bs = basisFactory.makeSmoothTemporalBasis('raised cosine', 1000, 10, binfun); % 1000ms
% offset = 0;
% dspec = buildGLM.addCovariateTiming(dspec, 'targon', [], [], bs, offset);

% % dots off
% bs = basisFactory.makeSmoothTemporalBasis('raised cosine', 500, 10, binfun); % 500ms
% offset = 0;
% dspec = buildGLM.addCovariateTiming(dspec, 'dotsoff', [], [], bs, offset);

% % Coherence dependent stimulus box car
% bs = basisFactory.makeSmoothTemporalBasis('raised cosine', 1000, 10, binfun); % 1000ms
% stimHandle = @(trial, expt) trial.coh * basisFactory.boxcarStim(binfun(trial.dotson), binfun(trial.dotsoff), binfun(trial.duration));
% dspec = buildGLM.addCovariate(dspec, 'cohKer', 'coh-dep dots stimulus', stimHandle, bs);

if isempty(varargin{2})
    % continue
elseif varargin{2} == 'integrator'
    if strcmp(modelType,'constant')
        % Add constant model
        stimHandle = @(trial,expt) basisFactory.constantStim(trial.t0,binfun(trial.duration)); % here trial.t0 is firing rate constant
        dspec = buildGLM.addCovariate(dspec, 'integratorUP', 'add ramp or step model', stimHandle, [], [],@(trial) (trial.choice == 1)); % basisStruct, offset, cond
        dspec = buildGLM.addCovariate(dspec, 'integratorDOWN', 'add ramp or step model', stimHandle,[],[],@(trial) (trial.choice == 2)); % basisStruct, offset, cond
    else
        % Add ramp/step model
        if (nargin < 7)
            stimHandle = @(trial, expt) basisFactory.rampstepStim(binfun(trial.dotson), binfun(trial.t0),binfun(trial.duration),modelType);
        elseif abs(varargin{3}) == 'signed'
            stimHandle = @(trial,expt) basisFactory.rampstepStim(binfun(trial.dotson),binfun(trial.t0),binfun(trial.duration),modelType,trial.modelsign);            
        end
        % dspec = buildGLM.addCovariate(dspec, 'integrator', 'add ramp or step model', stimHandle); 
        % trial.choice modulates in/out-RF
        dspec = buildGLM.addCovariate(dspec, 'integratorUP', 'add ramp or step model', stimHandle, [], [],@(trial) (trial.choice == 1)); % basisStruct, offset, cond
        dspec = buildGLM.addCovariate(dspec, 'integratorDOWN', 'add ramp or step model', stimHandle,[],[],@(trial) (trial.choice == 2)); % basisStruct, offset, cond
    end
end

% Compile the data into 'DesignMatrix' structure
trialIndices = (1:glmData.nTrials); % use all trials from current cell
dm = buildGLM.compileSparseDesignMatrix(dspec, trialIndices); % adds constant column 
% Get the spike trains back to regress against
[y,endTrialIndices] = buildGLM.getBinnedSpikeTrain(expt, 'sptrain', dm.trialIndices); % bins x 1 of concatenated spike trains across all trials
