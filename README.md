This MATLAB code is a modified implementation of Memming's neuroGLM package.
Forms the basis of a generalized flexible regression analyses of trial-based spike train data using a Generalized Linear Model (GLM). This modeling framework aims to discover how neural responses encode both external (e.g., sensory, motor, reward variables) and internal (e.g., spike history, LFP signals) covariates of the response.


