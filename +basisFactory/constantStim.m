function stim = constantStim(rate,nT)
% Returns either a ramp or step function during stimulus design 
% INPUT: dotson, step/ramp t0, total bins, choice (1,2)


stim = rate*[ones(1,nT)]';   