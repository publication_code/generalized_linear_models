function X = convBasis(stim, bases, offset)
% Convolve basis functions to the covariate matrix
%
%   stim: [T x dx] - dx covariates over time
%   bases: Basis structure
%   offset: [1] optional - shift in time

if nargin < 3
    offset = 0;
end

[~, dx] = size(stim);  % dx is dimension 

% zero pad stim to account for the offset
if offset < 0 % anti-causal
    stim = [stim; zeros(-offset, dx)]; % append zeros to end of vertical cols
elseif offset > 0; % push to future
    stim = [zeros(offset, dx); stim]; % append zeros to start of vertical cols
end

if issparse(stim) || nnz(stim) < 20; % number nonzero elements
    X = basisFactory.temporalBases_sparse(stim, bases.B); % convolve bases with stimulus
else
    X = basisFactory.temporalBases_dense(stim, bases.B);
end

if offset < 0 % anti-causal
    X = X(-offset+1:end, :); % lops offset from the beginning
elseif offset > 0
    X = X(1:end-offset, :); % cut off offset from the end
end