function stim = deltaStim(bt, nT, v)
% Returns a sparse vector with events at binned timings
% bt: bin times for spikes
% nT: final bin
%  v: 
bidx = bt <= nT; % bin idx of all spikes less than final bin
bt = bt(bidx); % ignore the events after nT bins; 1 for spike, 0 for ignore

o = ones(numel(bt), 1);

if nargin < 3
    v = o;
else
    v = v(bidx);
end

assert(numel(o) == numel(v));

stim = sparse(bt, o, v, nT, 1); % bt, o forms nT,1 matrix; stim(bt(k),o(k)) = v(k); 
% vector of ones at bins with spikes