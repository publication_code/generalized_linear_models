function stim = boxcarStim(startBinnedTime, endBinnedTime, nT)
% Returns a boxcar duration stimulus design

idx = startBinnedTime:endBinnedTime;
o = ones(numel(idx), 1);
stim = sparse(idx, o, o, nT, 1);

% Returns either a ramp or step function during stimulus design 

idx = startBinnedTime:endBinnedTime; % index of ramp/step function
o = ones(numel(idx),1);              % values at index
stim = sparse(idx,o,o,nT,1);         % (nT,1) matrix; idx(k), o(k) = o(k)
