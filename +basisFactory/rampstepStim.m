function stim = rampstepStim(startBinnedTime,t0BinnedTime,nT,model,varargin)
% Returns either a ramp or step function during stimulus design 
% INPUT: dotson, step/ramp t0, total bins

idx = startBinnedTime:t0BinnedTime; 
if (nargin < 5)
    sign = 1;
elseif abs(varargin{1}) == 1
    sign = varargin{1};
end

if model == 'ramp'
    stim = sign*[zeros(1,startBinnedTime-1) ,linspace(0,1,numel(idx)), ones(1,nT-t0BinnedTime)]'; % needs to be column
elseif model == 'step'
    stim = sign*[zeros(1,t0BinnedTime-1), ones(1,nT-t0BinnedTime+1)]';   
else 
    error('Integration model not recognized.')
end 
    stim = sparse(stim); 
% if choice == 1 
%     % choice is into RF
%     stim = sparse(stim);       
% elseif choice == 2
%     % choice is out of RF
%     stim = -sparse(stim);
% end


